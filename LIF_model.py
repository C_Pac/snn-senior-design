
# coding: utf-8

# In[ ]:

'''
Simplified LIF model to experiment w/ NetworkX and Brian2

Current status on runtime:
~75 seconds for n=200, m=300 at 200 ms initially
On consecutive runs, time reduces to ~1 second under same conditions == hysteresis effect? 
Want to collect times once reach steady-state, so have to keep running until runtime difference is not significant
    and that's what you would record and compare with FPGA (would need to keep testing CPU and specs constant)

For FPGA testing:
1. Change any random values to specific values (rand_temp,W,G.v,PoissonInput)
2. Figure out what variables need to be saved and sent to FPGA (c_rows,c_cols)

Manipulatable variables:
S3.connect
S3.w
PoissonInputs
G_.v
tau_m1
tau_m2
p_couple
w_couple

Variables of interest:
rows = source neuron numbers (numpy.ndarray)
cols = target neuron numbers (numpy.ndarray)
connect_W = weight array (numpy.ndarray)
A_temp5 = adjacency matrix (numpy.ndarray)
myW = weight adjacency matrix (numpy.ndarray)
c_rows = manually defined source neurons from coupling
c_cols = manually defined target neurons from coupling
coup_mat = coupling matrix (source x target)
spikemon1.t = spike trains for source network
spikemon2.t = spike trains for target network
rand_seed = seed to initiate randomization

Final output variables for Synapses group: 
ADJ matrix = rows (source), cols (target), A_temp5 (matrix form), c_rows (coupled), c_cols (coupled)
WT matrix = connect_W (weights between source-target in order of (rows,cols))
WT-ADJ matrix = myW (shows entire n*n matrix w/ E/I/non-connections) 

Work in progress for computer simulation:
0) Is there a workable threshold for number of neurons and connections?
    - Try establishing a maximum number of n and m before kernel crashes
    - Why does it appear that the top 80% of neurons are behaving differently (if n=250, n_weird=200->250)?
        - Yup, it's because excit_num
        - Inhibitory neurons have significantly decreased firing rate, but it shouldn't be that?
1) By changing coupling weight (S3.w), how does synchronicity change (see HH paper when they changed conductance & Fred 
        email 2/20)
    - One run function to go through different values of coupling weight and output many graphs
        OR individually go through different run functions that have specified coupling weight values and output 1 graph at a time
2) Once we have correct parameters, we can start manipulating different network parameters/topology
    - Would want different topology/parameters for each source/target network because they wouldn't be homogeneous 
3) Cleaner user interface where you only have to change the function instead of the source code

Work in progress for FPGA simulation:
1) Change values to match FPGA, e.g. 16-bit precision (dtype=int16) (?)

Example of synchronicity:
https://www.youtube.com/watch?v=yVkdfJ9PkRQ
''' and None


# In[1]:

import networkx as nx
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import numpy as np
import time
from brian2 import *
import pyspike as spk # Necessary package PySpike 
    # sudo pip install pyspike
import seaborn as sns # Necessary package: Seaborn
    # pip install git+git://github.com/mwaskom/seaborn.git#egg=seaborn
from scipy.stats.stats import pearsonr
#%matplotlib inline 


# In[2]:

class AdjacencyMatrix:  
    '''
    Function 1: Weighted adjacency matrix
    Call to initiate adjacency matrix
    Call to choose which neural network topology with given parameters
    
    Description:
    Given parameters, constructs network with adjacency matrix and applies random weights.
    
    Returns:
        G: NetworkX Graph
        A: Adjacency matrix. Sparse matrix
        rows: Presynaptic neurons
        cols: Postsynaptic neurons
        connect_W: Weights for each E/I connection (in order of rows,cols)
    
    Parameters:
        n: nodes
        m: edges
        k: neighbor connections
        p: probability 
        d: degrees
    '''
    def __init__(self,n): 
        plt.clf() # Clears any previous figures
        plt.close() # Clears any figure windows
        
    def all_to_all(self,n):
        G = nx.complete_graph(n) 
        #nx.draw(G, with_labels=True) # Draws plot with node labels
        #plt.savefig("All to all.png") # If want to save topology image
        A = nx.adjacency_matrix(G) # Outputs unit adjacency matrix
        return A, G

    def random(self,n,m): 
        # Interchangeable based on UI for different types of topography
        G = nx.dense_gnm_random_graph(n,m) # Uses NetX to generate random topography
        #nx.draw(G, with_labels=True) # Draws connectivity figure
        #plt.savefig("Random.png") # Saves connectivity figure as Random.png

        # Extracts ADJACENCY MATRIX from topography and rearranges to manageable array of (n*n) elements
        A = nx.adjacency_matrix(G) # Assigns A as adjacency matrix (which nodes are connected)
        return A, G 
    
    def small_world(self,n,k,p): 
        G = nx.newman_watts_strogatz_graph(n,k,p) 
        #nx.draw(G, with_labels=True)
        #plt.savefig("Small-world.png")
        A = nx.adjacency_matrix(G)
        return A, G
    
    def regular(self,d,n): 
        G = nx.random_regular_graph(d,n)
        #nx.draw(G, with_labels=True)
        #plt.savefig("Regular.png")
        A = nx.adjacency_matrix(G)
        return A, G
    
    def scale_free(self,n): 
        G = nx.scale_free_graph(n) 
        #nx.draw(G, with_labels=True)
        #plt.savefig("Scale free.png")
        A = nx.adjacency_matrix(G)
        return A, G

    def Weighted(self,A,n):
        A_temp1 = A.todense() # Converts A to manageable matrix
        A_temp2 = np.reshape(A_temp1,(1,n**2)) # Reshapes adjacency matrix to array for calculation
        A_temp3 = np.array(A_temp2) # Changes matrix element to type:array for calculation
        A_temp4 = A_temp3[0] # Selects the first and only cell in array for manipulation

        # Generates random values for n neurons to decide whether E/I
        rand_temp=np.random.rand(1,n) 
        rand_temp=rand_temp[0]

        # Changes positive to negative weights based on probability (***not necessary if define E/I in Synapses and subG)
        if 0:
            newlist = [] 
            for item in rand_temp:
                if item > 0.8: # Random numbers to negative according to uniform probability
                    item = -item 
                newlist.append(item) 

        # Reshapes adjacency matrix to workable matrix of n*n neurons
        A_temp5 = np.reshape(A_temp4,(n,n)) 

        # Generates random weights for each connection (assuming all-all) w/o self-feedback
        #W = np.random.rand(n,n) 
        W = np.random.randint(100, size=[n,n])
        W_n = W
        np.fill_diagonal(W,0) # Neurons are not self-connected
        myW = W

        # Prep for calculation of final weight matrix (basically just replacing all '1' in ADJ mat w/ corresponding WT mat)
        A_temp=np.reshape(A_temp5,(n**2,1)) # Reshapes A for multiplication
        W_temp=np.reshape(W,(1,n**2)) # Reshapes W for multiplication
        W=A_temp * W_temp # Makes (n**2)x(n**2) matrix
        W=W.diagonal() 
        W=np.reshape(W,(n,n)) 
        
        # Gets the index values for source(rows)/target(cols) neurons 
        rows, cols = np.nonzero(A_temp5)

        # Gets rid of duplicate connections (bidirectional --> unidirectional)
        new_coord = zip(rows,cols)
        #print new_coord
        new_rows = set(tuple(sorted(l)) for l in new_coord)
        g = np.array(list(new_rows))
        rows = g[:,0] #neurons i
        cols = g[:,1] #neurons j
        new_coord = zip(rows,cols)

        # To duplicate values above diagonal onto spots below diagonal
        for x in range(len(myW)):
            for y in range(len(myW)):
                myW[y,x] = myW[x,y]

        # Generates weight matrix in array that's necessary for Synapses in Brian2
        # Values are in order of new_coord array
        connect_W = []
        for i in range(len(new_coord)):
            connect_W.append(W[new_coord[i]])
        connect_W = np.array(connect_W) # Weights for each connection in ndarray

        # Changes weights (in array) to inhibitory if coming from an inhibitory neuron (upper 20% of n)
        excit_num = int(0.8*n) # Index for 0:excitatory neurons
        for x in range(len(new_coord)):
            for y in (0,1):
                if new_coord[x][y] >= excit_num: # Any neuron index greater than excit_num is inhibitory
                    connect_W[x] = -connect_W[x]
                    #final_W.append(connect_W(x))
                    
        # Changes weights (in matrix) to inhibitory if coming from an inhibitory neuron (upper 20% of n)
        for x in range(len(myW)):
            for y in range(len(myW)):
                if x >= excit_num:
                    myW[x,y] = -myW[x,y]
                if y >= excit_num:
                    myW[x,y] = -myW[x,y]
        
        return W, rows, cols, connect_W, new_coord, A_temp5, myW


# In[3]:

class Visualization:
    '''
    Function 2: Visualize neural network
    Inputs graph G 
    Returns cluster coefficient & characteristic path length
        & plot of connections between neurons (color-coded)
    For more info: see collective dynamics paper
    
    Description:
    From network model, determines cluster coefficient and characteristic path length for each
        node. For each network, will take average of those values, respectively and yield 
        single integer value.
    From network model, will output plot of connections, color-coded for excitatory and
        inhibitory.
    
    Returns:
        cc_avg: Cluster coefficient averaged over all nodes
        ex_in_plot: Plot of colored excitatory/inhibitory connections
        cpl_avg: Number of edges at shortest path over all nodes 
        
    Parameters:
        G: NetworkX Graph from Function 1
    '''
    def __init__(self):
        #plt.clf() # Clears any previous figures
        plt.close() # Clears any figure windows

    def cluster_coeff(self,G):        
        cc = nx.clustering(G)

        cc_y=[]
        for idx in cc:
            cc_y=np.append(cc_y,cc[idx])
        
        cc_avg = np.ndarray.mean(cc_y, dtype=np.float64)
        return cc_avg
    
    def ex_in_connec(self,G,connect_W):
        plt.figure()
        red_patch = mpatches.Patch(color='red', label='Excitatory')
        blue_patch = mpatches.Patch(color='blue', label='Inhibitory')
        plt.legend(handles=[red_patch,blue_patch])
        suptitle('Structural Connections', fontsize=14, fontweight='bold')

        edges = G.edges()
        nodes = G.nodes()

        custom_color={}
        for idx in range(len(connect_W)):
            if connect_W[idx] < 0:
                inhib_edge = new_coord[idx]
                G.add_edge(*inhib_edge)
                custom_color[inhib_edge]='b'
            else:
                excit_edge = new_coord[idx]
                G.add_edge(*excit_edge)
                custom_color[excit_edge]='r'
        if 0:
            for idx,idy in enumerate(edges):
                x1,y1 = edges[idx]
                if connect_W < 0:
                    inhib_edge = (x1,y1)
                    G.add_edge(x1,y1)
                    custom_color[x1,y1]='b' # Stores color of edges in dict
                else:
                    excit_edge = (x1,y1)
                    G.add_edge(x1,y1)
                    custom_color[x1,y1]='r'
        
        ex_in_plot=nx.draw_networkx(G,node_color='w',
                         with_labels=True,
                         node_list=nodes,
                         #node_size=50,
                         node_size=200,
                         edge_list=custom_color.keys(),
                         edge_color=custom_color.values(),
                         label='Blue=Inhibitory, Red=Excitatory')
        #plt.savefig("Structural Connections.png")
        
    def char_path_len(self,G):
        cpl = nx.all_pairs_shortest_path_length(G)
        my_array = []
        my_key = []
        cpl_count = []
        for idx in cpl:
            myarray = cpl[idx]
            min_val = min(ii for ii in myarray if ii > 0) # Find min length
            for key,length in myarray.iteritems():
                if length == min_val:
                    my_key = np.append(my_key,key)
            my_count = len(my_key) # Find number of edges of that length
            cpl_count = np.append(cpl_count,my_count)
            my_key = []
            cpl_avg = np.mean(cpl_count) # Find average of those edges
        return cpl_avg


# In[4]:

class BrianVisualization:
    '''
    Function 4: Visualization of Brian 
    Define LIF neural population in Brian
    Call to save spike times
    Call to plot voltage monitor
    Call to plot raster plot
    Call to plot histogram
    
    Description:
    Will plot the voltage monitor, raster plot, and histogram of neural network
    
    Returns:
        G: NeuronGroup
        spike_times: Spike times for neuron 0
        all_spikes: Spike times for all neurons
        
    
    Parameters:
        statemon: StateMonitor
        spikemon: SpikeMonitor
        run_time: Simulation run time
    
    '''
    def __init__(self):
        plt.clf() # Clears any previous figures
        plt.close() # Clears any figure windows
        
        start_scope()
    
    def network1(self,rows,cols,connect_W,N):
        '''
        For full synch: G1.v = fixed
                        PI = off
        '''
        eqs = '''
        dv/dt = (I-v)/tau : 1 (unless refractory)
        I : 1
        tau : second
        '''
                
        G1 = NeuronGroup(N, eqs, threshold='v>v_th', reset='v=v_r', refractory=10*ms, method='linear')
        G1.v = 'rand()' #random so changes dynamics for each neuron --> causes difference in raster
        #G1.v = '0.967188882214'
        '''
        Injection current is constant but with slight perturbations from PoissonInput, if that function is active
        To get rid of highly synchronized, G1.v='rand()' and turn on P1
        '''
        G1.I = I_c # Constant current 
        G1.tau = tau_m1 * ms
        
        # PoissonInput injection current -- changes each neuron's firing rate
        # Each neuron has different input current depending on Poisson distribution
        PI_num = 0.8*N 
        #subG1 = G1[int(PI_num):] # Top 20% of total neurons stimulated
        subG1 = G1[:] # All neurons stimulated via Poisson 
        '''
        PoissonInput(target,target_var,N,rate,weight)
        target: which neurons to send PoissonInput
        target_var: which variable that is being changed from input
        N: number of inputs (more input = higher firing rate)
        rate: rate of input (100Hz = 10ms per spike)
        weight: amount added to voltage
        '''
        P1 = PoissonInput(subG1, 'v', 5, 100*Hz, weight=0.1) # PoissonInput on
        #P1 = PoissonInput(subG1, 'v', 5, 100*Hz, weight=0) # PoissonInput off

        
        S1 = Synapses(G1, G1, 'w : 1', on_pre='v_post += w')
        S1.connect(i=rows, j=cols) # Adjacency matrix from Adj.weighted
        S1.w = connect_W/float(100) # Weighted matrix 
                
        return G1,S1,P1
    
    def network2(self,rows,cols,connect_W,N):
        '''
        Start off w/ identical network parameters as network 1, but need to eventually change connect_W (its interconnections)
        If P2 turned on, may need to increase S3.w so network 1 influence is higher than PoissonInput
        '''
        eqs = '''
        dv/dt = (I-v)/tau : 1 (unless refractory)
        I : 1
        tau : second
        '''

        G2 = NeuronGroup(N, eqs, threshold='v>v_th', reset='v=v_r', refractory=10*ms, method='linear')
        #G2.v = '0.967188882214' # For debugging of coupling so that all nodes in G2 will fire at same rate
        G2.v = 'rand()'
        G2.I = I_c
        G2.tau = tau_m2 * ms
        
        subG2 = G2[:]
        P2 = PoissonInput(subG2, 'v', 5, 100*Hz, weight=0.1)
        
        S2 = Synapses(G2, G2, 'w:1', on_pre='v_post += w')
        S2.connect(i=rows, j=cols) # Network 2 has same inter-network connections as Network 1
        S2.w = connect_W/float(100)
        
        return G2,S2,P2

    def network_coupling(self,N,p_couple,w_couple,G1,G2):
        '''
        Should see how coupling between different subpopulation has global effects (raster plot)
            - Could see difference if neurons have same firing rate (non-PoissonInput) vs. different firing rate (all-PoissonInput)
            - May only want to record (Statemon, Spikemon) from this last coupling (G2) to save resources
                - See Monitoring Synaptic Variables from http://brian2.readthedocs.io/en/2.0.1/user/synapses.html
            = Can introduce multiple output synapses (multisynaptic_index from http://brian2.readthedocs.io/en/2.0.1/user/synapses.html)
                - Or more simply "S.connect(i=numpy.arange(10), j=1)"
        '''
        S3 = Synapses(G1,G2, 'w:1', on_pre='v_post += w')#, delay=5*ms) # G1 drives G2
        
        ### Manually defining coupling ###
        p_couple2 = p_couple*N
        i_couple = 0.8*N
        
        # If want 1:1 for only first p_couple% neurons (excitatory --> excitatory)
        c_rows = list(arange(0,p_couple2,dtype=int)) # Source neurons
        c_cols = list(arange(0,p_couple2,dtype=int)) # Target neurons
        
        # If want 1:1 for only last p_couple% neurons 
        #c_rows = list(arange(N-p_couple2,N,dtype=int))
        #c_cols = list(arange(N-p_couple2,N,dtype=int))
        
        # If want 1:1 for inhibitory onto inhibitory neurons
        #c_rows = list(arange(N-i_couple,N,dtype=int))
        #c_cols = list(arange(N-i_couple,N,dtype=int))        
        
        # If want 1:1 for projection of excitatory onto inhibitory neurons
        #c_rows = list(arange(0,p_couple2,dtype=int))
        #c_cols = list(arange(N-i_couple,N,dtype=int))
        
        # If want 1:! for projection of inhibitory onto excitatory neurons
        #c_rows = list(arange(N-p_couple2,N,dtype=int))
        #c_cols = list(arange(0,p_couple2,dtype=int))
        
        S3.connect(i=c_rows, j=c_cols) # Manually defined coupling
        S3.w = w_couple
        ###################################
        
        ##### Probabilistic coupling #####
        #S3.connect(p=0.05) # Probabilistic connection - Chance that G2 will connect with and spike from G1
        #S3.w = 0.02
        #S3.connect(p=p_couple)
        ###################################
                
        # Coupling matrix
        coup_mat = [[0 for x in range(N)] for y in range(N)]

        for ii in range(len(c_rows)):
            for jj in range(len(c_cols)):
                coup_mat[ii][ii] = 1

        statemon1 = StateMonitor(G1, 'v', record=0) # Records just neuron 0 to save resources
        spikemon1 = SpikeMonitor(G1, variables='v')
        statemon2 = StateMonitor(G2, 'v', record=0) # Records just neuron 0 to save resources
        spikemon2 = SpikeMonitor(G2, variables='v')
                
        run(run_time*ms, 'text')

        return statemon1,spikemon1,statemon2,spikemon2,c_rows,c_cols,coup_mat
        
    def spike_time(self,spikemon):
        all_values = spikemon.all_values()
        spike_times = all_values['t'][0] # Spike times for just neuron 0
        all_spikes = spikemon.t/ms # Spike times for all neurons
        
        return spike_times,all_spikes
        
    def voltage_monitor(self,statemon):
        plot(statemon.t/ms, statemon.v[0])
        #plot(statemon.t/ms, statemon.v[1])  # Plots second neuron      
        ylabel('Voltage (V)')
        xlabel('Time (ms)')
        
    def raster_plot(self,spikemon,spikemon_other):
        #ion()
        plot(spikemon.t/ms, spikemon.i, '.r')
        plot(spikemon_other.t/ms, spikemon_other.i, '.k') # Plots overlay of each network
        xlabel('Time (ms)')
        ylabel('Neuron index');
        #plt.show(block=True)
        
    def spike_hist(self,run_time,all_spikes):
        my_bins = arange(0,run_time+2,2)
        plt.hist(all_spikes, bins=my_bins)
        xlabel('Time (ms)')
        ylabel('Total number of spikes')


# In[101]:

class SynchronicityCalculation:
    '''
    To calculate different metrics of synchronicity
    
    For more information:
        See Synch Metrics bookmarks folder
        http://wwwold.fi.isc.cnr.it/users/thomas.kreuz/sourcecode.html
        https://arxiv.org/pdf/1603.03293.pdf
        http://mariomulansky.github.io/PySpike/pyspike.html#pyspike.SpikeTrain.SpikeTrain
        http://mariomulansky.github.io/PySpike/index.html
        http://www.scholarpedia.org/article/Measures_of_spike_train_synchrony#ISI-distance
    '''
    def __init__(self):
        plt.clf() # Clears any previous figures
        plt.close() # Clears any figure windows

    def Initialize(self,spikemon1,spikemon2):
        st1 = spk.SpikeTrain(list(spikemon1.t/ms), edges=[0,run_time])
        st2 = spk.SpikeTrain(list(spikemon2.t/ms), edges=[0,run_time])

        return st1,st2

    def SPIKEsynch(self,spikemon1,spikemon2):
        '''
        SPIKE-synchronization measures similarity where 0 means absence of synchrony and bounded to 1
        indicating absolute synchrony
        '''
        spike_sync = spk.spike_sync([st1,st2])
        #print spike_sync

        # Plotting SPIKE-synchronicity
        spike_profile = spk.spike_sync_profile([st1,st2])
        x,y = spike_profile.get_plottable_data()
        plot(x,y,'-k')
        ylabel('SPIKE-sync')

    def ISIdistance(self,spikemon1,spikemon2):
        '''
        ISI-distance quantifies dissimilarity based on differences of interspike intervals from two
        different spike trains. Becomes 0 for identical spike trains and approaches -1 and 1 when
        first or second spike train is faster than the other, respectively.
        '''
        isi_prof = spk.isi_profile(st1,st2)
        isi_dist = isi_prof.avrg()
        #print isi_dist # Outputs nan if spike train has same time values

        # Plotting ISI profile
        x,y = isi_prof.get_plottable_data()
        plot(x,y,'-k')
        ylabel('ISI')

    def SPIKEdistance(self,spikemon1,spikemon2):
        '''
        SPIKE-distance quantifies dissimilarity based on exact spike timings. In other words,
        dissimilarity in terms of deviations from exact coincidences of spikes
        Becomes 0 for identical spike trains, and bounded by 1 for highly dissimilar
        '''
        spike_dist = spk.spike_distance([st1,st2])
        #print spike_dist

        spike_profile = spk.spike_profile([st1,st2])
        x,y = spike_profile.get_plottable_data()
        plot(x,y,'-k')
        xlabel('Time (ms)')
        ylabel('SPIKE-dist')

    def CrossCorrelation(self,spikemon1,spikemon2,bin_time):
        #my_bins = arange(0,run_time+2,2) # 2 ms bins
        #my_bins = arange(0,run_time+1,1) # 1 ms bins
        #my_bins = arange(0,run_time+0.05,0.05) # 0.05 ms bins
        my_bins = arange(0,run_time+bin_time,bin_time) # 0.2 ms bins
        
        # Extract binary vector where 1 if spike within given time bin, else 0
        my_spikes1 = spikemon1.t/ms
        plt.figure(10)
        n1 = plt.hist(my_spikes1, bins=my_bins)
        my_vec1 = n1[0]
        for ii in range(len(my_vec1)):
            if my_vec1[ii] >= 1:
                my_vec1[ii] = 1

        my_spikes2 = spikemon2.t/ms #+ 100
        plt.figure(11)
        n2 = plt.hist(my_spikes2, bins=my_bins)
        my_vec2 = n2[0]
        for ii in range(len(my_vec2)):
            if my_vec2[ii] >= 1:
                my_vec2[ii] = 1 
        
        plt.close(10)
        plt.close(11)
        #plt.clf() # Clears histogram plots

        # Normalize spike times
        norm1 = my_vec1 / np.linalg.norm(my_vec1)
        norm2 = my_vec2 / np.linalg.norm(my_vec2)
        my_vec_norm1 = norm1
        my_vec_norm2 = norm2

        # Find correlation
        coup_corr = np.correlate(my_vec_norm1,my_vec_norm2,"full") 
        self_corr = np.correlate(my_vec_norm2,my_vec_norm2,"full") 
    
        # Plotting correlation
        x_val_coup_corr = range(len(coup_corr))
        x_val_self_corr = range(len(self_corr))
        #plot(x_val_coup_corr-np.argmax(self_corr/ms),coup_corr,'b') # Shifted
        #plot(x_val_self_corr-np.argmax(self_corr/ms),self_corr,'g') # Shifted
        plot(x_val_coup_corr,coup_corr,'b') # Non-shifted
        plot(x_val_self_corr,self_corr,'g') # Non-shifted
        blue_patch = mpatches.Patch(color='blue', label='Test Correlation: net1-net2')
        green_patch = mpatches.Patch(color='green', label='Autocorrelation: net2-net2')
        suptitle('Comparing networks', fontsize=14, fontweight='bold')
        plt.legend(handles=[blue_patch,green_patch])
        
    def CorrelationMatrix(self,spikemon1,spikemon2,bin_time):
        my_bins = arange(0,run_time+bin_time,bin_time)*ms

        # Get spike train data from SpikeMonitor that is ordered by neuron index
        spike_trains1 = spikemon1.spike_trains()
        spike_trains2 = spikemon2.spike_trains()

        # Convert spike trains for each population to list
        list_spike_train1 = [ v for v in spike_trains1.values() ]
        list_spike_train2 = [ v for v in spike_trains2.values() ] # Further differentiate into spike trains for indiv. neuron?

        array_spike_train1 = np.array(list_spike_train1)
        array_spike_train2 = np.array(list_spike_train2)

        # Generalized binary vectors for entire array_spike_train1
        binary_vec1 = [None]*len(array_spike_train1) # Initialization of variable

        for jj in range(len(array_spike_train1)):
            my_spikes1 = array_spike_train1[jj] # Looking at individual neurons
            plt.figure(10)
            n1 = plt.hist(my_spikes1, bins=my_bins)
            my_vec1 = n1[0] # Values of histogram bins for each neuron
            for ii in range(len(my_vec1)):
                if my_vec1[ii] >= 1:
                    my_vec1[ii] = 1
            binary_vec1[jj] = my_vec1

        # Generalized binary vectors for entire array_spike_train2
        binary_vec2 = [None]*len(array_spike_train2)

        for jj in range(len(array_spike_train2)):
            my_spikes2 = array_spike_train2[jj]
            plt.figure(10)
            n2 = plt.hist(my_spikes2, bins=my_bins)
            my_vec2 = n2[0]
            for ii in range(len(my_vec2)):
                if my_vec2[ii] >= 1:
                    my_vec2[ii] = 1
            binary_vec2[jj] = my_vec2

        plt.close(10)
        R = np.corrcoef(binary_vec1,binary_vec2) # Numpy correlation coefficient matrix
        f,ax = plt.subplots(figsize=(12,9))
        my_corr = sns.heatmap(R,vmin=0,vmax=1,square=True)
        ax.set_title('Pairwise Correlation Heatmap')
        f.tight_layout()


# In[106]:

'''
Sample function 1 calling
Change parameters to fit neural model
Computes weighted adjacency matrix

Parameters to be user-defined:
    n: nodes
    m: edges
    k: each node is connected to k nearest neightbors
    p: probability of adding new edge for each edge
    d: degree of each node
'''
# Sample call function
n = 20 # Kills kernel at n = 250, m=375 possibly b/c cpu limitations
m = 30
k = 2
p = 0.2
d = 2
rand_seed = np.random.seed(int(time.time())) # To seed random number generator based on time

Adj = AdjacencyMatrix(n) # Initiates 

#[A,G] = Adj.all_to_all(n) # Defines all-to-all topology
[A,G] = Adj.random(n,m) # Defines random topology
#[A,G] = Adj.small_world(n,k,p) # Defines small-world topology
#[A,G] = Adj.regular(d,n) # Defines regular topology
#[A,G] = Adj.scale_free(n) # Defines scale-free topology

W,rows,cols,connect_W,new_coord,A_temp5,myW = Adj.Weighted(A,n) # Output

'''
Sample function 2 calling
First plot: cluster coefficient for each neuron
Second plot: excitatory (r) and inhibitory (b) connections
'''
vis = Visualization() # Initiates 
cc_avg = vis.cluster_coeff(G) # Calculates average cluster coefficient

vis.ex_in_connec(G,connect_W) # Plots excitatory/inhibitory connections
cpl_avg = vis.char_path_len(G) # Calculates average characteristic path length
print "Average characteristic path length:"
print cpl_avg

#show(block=True) # Show Network connections


# In[107]:

'''
Sample function 4 calling
Change parameters to fit neural model
Simulates leaky integrate-and-fire neuron model

Parameters:
    N: number of neurons
    tau_m: time constant (ms)
    v_r = reset membrane potential (mv)
    v_th = threshold membrane potential (mv)
    I_c = constant input current
    run_time = simulation time (ms)
    p_couple = probability that neuron i in first net will couple with neuron i in second net
    w_couple = coupling weight (influence from net1 to net2)
'''
N = n 
tau_m1 = 20.4 #37
tau_m2 = 32.4 #43
v_r = 0 
v_th = 1 
I_c = 2 
run_time = 300
p_couple = 0.99 #0.99 
w_couple = 0.5 #1 
'''
Coupling weight seems to influence how long two nets will stay coupled together
But it doesn't seem to look like there's a big difference between w=5 and w=5000
The difference is only clearly visible if network 1 has fixed voltage and PoissonInput off
'''
BrianVis = BrianVisualization() # Initiates

run_t = time.time() # Records initial runtime

[G1,S1,P1] = BrianVis.network1(rows,cols,connect_W,N) # Runs LIF model for first network
[G2,S2,P2] = BrianVis.network2(rows,cols,connect_W,N) # Runs LIF model for second network
[statemon1,spikemon1,statemon2,spikemon2,c_rows,c_cols,coup_mat] = BrianVis.network_coupling(N,p_couple,w_couple,G1,G2) # Couples first and second networks

elapsed = time.time() - run_t # Calculates elapsed runtime
print 'Total runtime:'
print elapsed # Prints elpased runtime


# In[170]:

'''
Calculating synchronicity metrics (SPIKE-synchronicity,ISI-distance,SPIKE-distance)
'''
Sync = SynchronicityCalculation() # Initiates
[st1,st2] = Sync.Initialize(spikemon1,spikemon2)

fig1 = plt.subplot(411)
suptitle('Synchronicity Metrics', fontsize=14, fontweight='bold')
BrianVis.raster_plot(spikemon2,spikemon1)
    # Red == Network2 (target network)
    # Black == Network1 (source network)

fig2 = plt.subplot(412,sharex=fig1)
Sync.SPIKEsynch(st1,st2) # Plots SPIKE-synchronization

fig3 = plt.subplot(413,sharex=fig1)
Sync.ISIdistance(st1,st2) # Plots ISI-distance

fig4 = plt.subplot(414,sharex=fig1)
Sync.SPIKEdistance(st1,st2) # Plots SPIKE-distance

figManager = plt.get_current_fig_manager()
figManager.window.showMaximized()

show(block=True)


# In[103]:

'''
Plotting cross-correlation
'''
bin_time = 0.2 # 0.2 ms bins

Sync = SynchronicityCalculation() # Initiates


suptitle('Correlations Between Networks', fontsize=14, fontweight='bold')

# If want overlaid plots -- Need to de-comment the self_corr from function
Sync.CrossCorrelation(spikemon1,spikemon2,bin_time)

# If want subplots 
if 0:
    fig1 = plt.subplot(221) # Top left: net1-net2
    coup_corr = Sync.CrossCorrelation(spikemon1,spikemon2,bin_time)
    fig1.set_title("Network 1-Network 2")

    #print coup_corr 
    #print len(coup_corr)

    fig2 = plt.subplot(222,sharey=fig1) # Top right: net2-net2
    coup_corr = Sync.CrossCorrelation(spikemon2,spikemon2,bin_time)
    fig2.set_title("Network 2-Network 2")

    fig3 = plt.subplot(223,sharey=fig1) # Bottom left: net1-net1
    coup_corr = Sync.CrossCorrelation(spikemon1,spikemon1,bin_time)
    fig3.set_title("Network 1-Network 1")

    fig4 = plt.subplot(224,sharey=fig1) # Bottom right: net2-net1 == should be same as net1-net2
    coup_corr = Sync.CrossCorrelation(spikemon2,spikemon1,bin_time)
    fig4.set_title("Network 2-Network 1")

figManager = plt.get_current_fig_manager()
figManager.window.showMaximized()

show(block=True)


# In[108]:

'''
Plotting pairwise correlation
'''
bin_time = 2

Sync = SynchronicityCalculation() # Initiates
Sync.CorrelationMatrix(spikemon1,spikemon2,bin_time)

show(block=True)


# In[105]:

'''
Plotting network dynamics (raster and PSTH)
'''

# Network 1 voltage monitor
#suptitle('Network 1 voltage monitor', fontsize=14, fontweight='bold')
#BrianVis.voltage_monitor(statemon1) # Plots voltage monitor

# Network 2 voltage monitor
#suptitle('Network 2 voltage monitor', fontsize=14, fontweight='bold')
#BrianVis.voltage_monitor(statemon2) # Plots voltage monitor

# Gather data for network 1
[spike_times,all_spikes] = BrianVis.spike_time(spikemon1)

# Network 1 raster
fig1 = plt.subplot(221)
suptitle('Two network raster and PSTH', fontsize=14, fontweight='bold')
BrianVis.raster_plot(spikemon1,spikemon1) 
    # Network 1 in black (second input)

# Network 1 PSTH
fig2 = plt.subplot(222, sharex=fig1)
BrianVis.spike_hist(run_time,all_spikes) 

# Gather data for network 2
[spike_times,all_spikes] = BrianVis.spike_time(spikemon2)

# Network 2 raster
fig3 = plt.subplot(223, sharex=fig1, sharey=fig1)
BrianVis.raster_plot(spikemon2,spikemon1) 
    # Network 2 in red (first input)
    # Network 1 in black (second input)

# Network 2 PSTH
fig4 = plt.subplot(224, sharex=fig1)
BrianVis.spike_hist(run_time,all_spikes)

# Qt4Agg backend for full screen window display
figManager = plt.get_current_fig_manager()
figManager.window.showMaximized()

show(block=True)


# In[26]:

'''
Attempt to reconfigure spike trains for better quantitation of synchronicity
'''

if 0:
    # Attempt to separate into n rows

    x1 = spikemon1.t/ms
    tot_col = len(x1)/N

    y1 = np.reshape(x,(N,tot_col),order='f') 
    #print y1[0]

    # Are there 42 spikes over the course of 1 sec? Yes

    x2 = spikemon2.t/ms
    tot_col = len(x1)/N

    #print len(x2)
    #print N

# Can't do spikemon2 like spikemon1 because the length is different and a neuron might have fired more often than another
    # Now we have to compare list_spike_train1 to list(spikemon1.t/ms) to figure out why won't load right into SpikeTrain

# Get spike train data from SpikeMonitor that is ordered by neuron index
spike_trains1 = spikemon1.spike_trains()
spike_trains2 = spikemon2.spike_trains()

# Convert spike trains for each population to list
list_spike_train1 = [ v for v in spike_trains1.values() ]
list_spike_train2 = [ v for v in spike_trains2.values() ] # Further differentiate into spike trains for indiv. neuron?

# Convert list of spike trains to list for each individual neuron
    # Would this corrupt the synchronicty bc not just comparing population to population, but comparing inter-population and intra-population variation

#convert to txt file where each line has spike times for each neuron
spike_train1_txt = open('Spikes1.txt','w')
for item in list_spike_train1:
    spike_train1_txt.write("%s\n" % item)
    
spike_train2_txt = open('Spikes2.txt','w')
for item in list_spike_train2:
    spike_train2_txt.write("%s\n" % item)

# Load spikes from saved txt file
my_st1 = spk.load_spike_trains_from_txt("Spikes1.txt",edges=[0,run_time])
my_st2 = spk.load_spike_trains_from_txt("Spikes2.txt",edges=[0,run_time])

test_st1 = spk.load_spike_trains_from_txt("PySpike_testdata.txt",edges=[0,4000])

# NxM matrix where N is number of neurons and M is number of spikes for each neuron
#isi_prof = spk.isi_profile(test_st1) # works for comparing 1 entire population (NxM matrix)
#isi_prof = spk.isi_profile(test_st1[0],test_st1[1]) # works for intra-population (1xM array)
#isi_prof = spk.isi_profile(test,st1,test_st1) # doesn't work for comparing inter-population (2 NxM matrices)
#isi_prof = spk.isi_profile(my_st1) # seems to be take a long time and yields 0
    
# Comparing single neurons ISI (same index of net1 to net2)
#isi_prof = spk.isi_profile(my_st1[0],my_st2[0]) # comparing coupled neurons
isi_prof = spk.isi_profile(my_st1[170],my_st2[170]) # comparing uncoupled neurons
isi_dist = isi_prof.avrg()
x,y = isi_prof.get_plottable_data()

# Plot of spike trains
fig1 = plt.subplot(211)
suptitle('Synchronicity Metrics', fontsize=14, fontweight='bold')
BrianVis.raster_plot(spikemon2,spikemon1)
    # Red == Network2 (target network)
    # Black == Network1 (source network)
    
# Plot of ISI-distance
fig2 = plt.subplot(212)
plot(x,y,'-k')
ylabel('ISI')
#axes = plt.gca()
#axes.set_ylim([0,0.6]) # set axes

figManager = plt.get_current_fig_manager()
figManager.window.showMaximized()

show(block=True)

if 0:
    # Second attempt - doesn't work
    outarr = np.vstack(list_spike_train1)
    np.savetxt("Spikes2.txt",outarr.T)

    # Third attempt - doesn't work
    import json
    with open('Spikes3.txt','w') as myfile:
        json.dump(list_spike_train1,myfile)

#my_st1 = spk.SpikeTrain(list(spikemon1.t/ms), edges=[0,run_time])

if 0:
    '''
    Calculating synchronicity metrics (SPIKE-synchronicity,ISI-distance,SPIKE-distance) with reshaped spikemon
    '''
    Sync = SynchronicityCalculation() # Initiates
    [st1,st2] = Sync.Initialize(spikemon1,spikemon2)

    fig1 = plt.subplot(411)
    suptitle('Synchronicity Metrics', fontsize=14, fontweight='bold')
    BrianVis.raster_plot(spikemon2,spikemon1)
        # Red == Network2 (target network)
        # Black == Network1 (source network)

    fig2 = plt.subplot(412,sharex=fig1)
    Sync.SPIKEsynch(st1,st2) # Plots SPIKE-synchronization

    fig3 = plt.subplot(413,sharex=fig1)
    Sync.ISIdistance(st1,st2) # Plots ISI-distance

    fig4 = plt.subplot(414,sharex=fig1)
    Sync.SPIKEdistance(st1,st2) # Plots SPIKE-distance

    figManager = plt.get_current_fig_manager()
    figManager.window.showMaximized()

    show(block=True)

