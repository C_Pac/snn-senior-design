
# coding: utf-8

# In[ ]:

'''
Hodgkin-Huxley model to observe how different connections influence network dynamics. This model draws influence
from COBAHH.py as reference:

Simulation of networks of spiking neurons: A review of tools and strategies (2006).
Brette, Rudolph, Carnevale, Hines, Beeman, Bower, Diesmann, Goodman, Harris, Zirpe,
Natschläger, Pecevski, Ermentrout, Djurfeldt, Lansner, Rochel, Vibert, Alvarez, Muller,
Davison, El Boustani and Destexhe.
Journal of Computational Neuroscience

https://github.com/brian-team/brian2/blob/master/examples/COBAHH.py

Work in progress:
1. Attempt to further reproduce results from following paper depicting how changing slow/fast connections 
influence network dynamics (see Fig. 6):

Inter-network interactions: impact of connections between oscillatory neuronal networks on oscillation 
frequency and pattern (2014).
Oscar J. Avella Gonzalez, Karlijn I. van Aerde, Huibert D. Mansvelder, Jaap van Pelt, Arjen van Ooyen

2. After reproducing results in paper, work on LIF_model to get equivalent network dynamics
''' and None


# In[1]:

from brian2 import * 
get_ipython().magic(u'matplotlib inline')
import random


# In[43]:


# Parameters
N=10 # Total number of neurons
excit_num = 0.8*N # Number of excitatory neurons

area = 20000*umetre**2  
Cm = (1*ufarad*cm**-2) * area 
gl = (1*psiemens*um**-2) * area # Leak conductance
g_na = (1000*psiemens*um**-2) * area # Sodium conductance
g_kd = (800*psiemens*um**-2) * area # Potassium conductance
El = -67*mV # Leak reversal potential
ENa = 50*mV # Sodium reversal potential
EK = -100*mV # Potassium reversal potential
VT = -63*mV # Threshold voltage

taue = 2*ms # Excitatory time constant
#taui = 6.8*ms # Inhibitory time constant for 'slow' network
taui = 3.5*ms # Inhibitory time constant for 'fast' network

Ee = 0*mV # Excitatory reversal potential
Ei = -80*mV # Inhibitory reversal potential
we = 6*nS  # Excitatory synaptic weight
wi = 67*nS  # Inhibitory synaptic weight

# The model
eqs = Equations(
    '''
    I_e = 0.2*nA : amp
    dv/dt = (I_e + gl*(El-v)+ge*(Ee-v)+gi*(Ei-v)-
             g_na*(m*m*m)*h*(v-ENa)-
             g_kd*(n*n*n*n)*(v-EK))/Cm : volt
    dm/dt = alpha_m*(1-m)-beta_m*m : 1
    dn/dt = alpha_n*(1-n)-beta_n*n : 1
    dh/dt = alpha_h*(1-h)-beta_h*h : 1
    dge/dt = -ge*(1./taue) : siemens
    dgi/dt = -gi*(1./taui) : siemens
    
    # Gonzalez et al. equations -- breaks simulation
    #alpha_h = 0.128*exp(-0.056*(volt**-1)*(v+50*mV+VT))/ms : Hz
    #alpha_m = 0.32*(volt**-1)*(54*mV+v+VT) / (1-exp(-0.25*(volt**-1)*(54*mV+v+VT)))/ms : Hz
    #alpha_n = 0.032*(volt**-1)*(52*mV+v+VT)/(1-exp(-0.2*(volt**-1)*(v+52*mV+VT)))/ms : Hz
    #beta_h = 4/(1+exp(-0.2*(volt**-1)*(v+27*mV-VT)))/ms : Hz
    #beta_m = 0.28*(volt**-1)*(27*mV+v-VT) / (exp(0.2*(volt**-1)*(v+27*mV-VT))-1)/ms : Hz
    #beta_n = 0.5*exp(-0.025*(volt**-1)*(57*mV+v-VT))/ms : Hz
    
    # Original equations
    alpha_m = 0.32*(mV**-1)*(13*mV-v+VT)/ (exp((13*mV-v+VT)/(4*mV))-1.)/ms : Hz
    beta_m = 0.28*(mV**-1)*(v-VT-40*mV)/ (exp((v-VT-40*mV)/(5*mV))-1)/ms : Hz
    alpha_h = 0.128*exp((17*mV-v+VT)/(18*mV))/ms : Hz
    beta_h = 4./(1+exp((40*mV-v+VT)/(5*mV)))/ms : Hz
    alpha_n = 0.032*(mV**-1)*(15*mV-v+VT)/ (exp((15*mV-v+VT)/(5*mV))-1.)/ms : Hz
    beta_n = .5*exp((10*mV-v+VT)/(40*mV))/ms : Hz
    ''')
P = NeuronGroup(N, model=eqs, threshold='v>-20*mV', refractory=10*ms,
                method='exponential_euler')

Pe = P[:int(excit_num)] # 80% excitatory
Pi = P[int(excit_num):] # 20% inhibitory
Cei = Synapses(Pe, Pi, on_pre='ge+=we') # E-I connections
Cie = Synapses(Pi, Pe, on_pre='gi+=wi') # I-E connections
Cee = Synapses(Pe, Pe, on_pre='ge+=we') # E-E connections
Cii = Synapses(Pi, Pi, on_pre='gi+=wi') # I-I connections
Cei.connect(p=0.65) # Probability for E-I connection
Cie.connect(p=0.6) # Probability for I-E connection
Cee.connect(p=0.3) # Probability for E-E connection
Cii.connect(p=0.55) # Probability for I-I connection

# Initialization 
P.v = 'El + (randn() * 6 - 5)*mV' 
P.ge = '(randn() * 1.5 + 4) * 10.*nS' 
P.gi = '(randn() * 12 + 20) * 10.*nS' 
    
# Record a few traces
trace = StateMonitor(P, 'v', record=[1,2])
spikemon = SpikeMonitor(P,variables='v')

run(0.5*second, report='text')

# Voltage monitor neuron 1
plt.figure(1)
plot(trace.t/ms, trace[1].v/mV)
xlabel('t (ms)')
ylabel('v (mV)')

# Voltage monitor neuron 2
plt.figure(2)
plot(trace.t/ms, trace[2].v/mV)
xlabel('t (ms)')
ylabel('v (mV)')

# Raster plot
plt.figure(3)
plot(spikemon.t/ms, spikemon.i, '.k')
xlabel('Time (ms)')
ylabel('Neuron index')

show()

