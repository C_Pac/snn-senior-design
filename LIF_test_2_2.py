
# coding: utf-8

# In[1]:

'''
Simplified LIF model for testing on FPGA
    - Eventually combine this code w/ LIF_model and add necessary changes, e.g. myW, elapsed, etc.

Changed:
n = 3
m = 3
rand_temp set to specific values
W set to specific values
G.v set to specific value
Got rid of PoissonInput -- each neuron has same firing rate (very regular and reproducible)
Added runtime calculation for calling of function 4
Added myW variable and made it equivalent above diagonal and below & corrected for negative inhibitory weights
Made myW a matrix of integers that is divided by 100 in order for Brian to assign weights

Variables of interest:
rows = source neuron numbers (numpy.ndarray)
cols = target neuron numbers (numpy.ndarray)
connect_W = weight array (numpy.ndarray)
A_temp5 = adjacency matrix (numpy.ndarray)
myW = weight adjacency matrix (numpy.ndarray) -- only care about values above diagonal b/c unidirectional connections
''' and None


# In[8]:

import networkx as nx
import matplotlib.pyplot as plt
import numpy as np
import time
from brian2 import *
get_ipython().magic(u'matplotlib inline')


# In[87]:

class AdjacencyMatrix:  
    '''
    Function 1: Weighted adjacency matrix
    Call to initiate adjacency matrix
    Call to choose which neural network topology with given parameters
    
    Description:
    Given parameters, constructs network with adjacency matrix and applies random weights.
    
    Returns:
        G: NetworkX Graph
        A: Adjacency matrix. Sparse matrix
        rows: Presynaptic neurons
        cols: Postsynaptic neurons
        connect_W: Weights for each E/I connection (in order of rows,cols)
    
    Parameters:
        n: nodes
        m: edges
        k: neighbor connections
        p: probability 
        d: degrees
    '''
    def __init__(self,n): 
        plt.clf() # Clears any previous figures
            
    def all_to_all(self,n):
        G = nx.complete_graph(n) 
        nx.draw(G, with_labels=True) # Draws plot with node labels
        #plt.savefig("All to all.png") # If want to save topology image
        A = nx.adjacency_matrix(G) # Outputs unit adjacency matrix
        return A, G

    def random(self,n,m): 
        # Interchangeable based on UI for different types of topography
        G = nx.dense_gnm_random_graph(n,m) # Uses NetX to generate random topography
        nx.draw(G, with_labels=True) # Draws connectivity figure
        #plt.savefig("Random.png") # Saves connectivity figure as Random.png

        # Extracts ADJACENCY MATRIX from topography and rearranges to manageable array of (n*n) elements
        A = nx.adjacency_matrix(G) # Assigns A as adjacency matrix (which nodes are connected)
        return A, G 
    
    def small_world(self,n,k,p): 
        G = nx.newman_watts_strogatz_graph(n,k,p) 
        nx.draw(G, with_labels=True)
        #plt.savefig("Small-world.png")
        A = nx.adjacency_matrix(G)
        return A, G
    
    def regular(self,d,n): 
        G = nx.random_regular_graph(d,n)
        nx.draw(G, with_labels=True)
        #plt.savefig("Regular.png")
        A = nx.adjacency_matrix(G)
        return A, G
    
    def scale_free(self,n): 
        G = nx.scale_free_graph(n) 
        nx.draw(G, with_labels=True)
        #plt.savefig("Scale free.png")
        A = nx.adjacency_matrix(G)
        return A, G

    def Weighted(self,A,n):
        A_temp1 = A.todense() # Converts A to manageable matrix
        A_temp2 = np.reshape(A_temp1,(1,n**2)) # Reshapes adjacency matrix to array for calculation
        A_temp3 = np.array(A_temp2) # Changes matrix element to type:array for calculation
        A_temp4 = A_temp3[0] # Selects the first and only cell in array for manipulation

        # Generates random values for n neurons to decide whether E/I
        #rand_temp=np.random.rand(1,n) 
        #rand_temp=rand_temp[0]
        rand_temp = np.array([ 0.43235183, 0.35933812, 0.10688581])

        # Changes positive to negative weights based on probability (***not necessary if define E/I in Synapses and subG)
        if 0:
            newlist = [] 
            for item in rand_temp:
                if item > 0.8: # Random numbers to negative according to uniform probability
                    item = -item 
                newlist.append(item) 

        # Reshapes adjacency matrix to workable matrix of n*n neurons
        A_temp5 = np.reshape(A_temp4,(n,n)) 

        # Generates random non-integer weights for each connection (assuming all-all) w/o self-feedback
        #W = np.random.rand(n,n) 
        #W = np.matrix( '0.20309858, 0.2607987, 0.69136377; 0.40748214, 0.0348424, 0.82106128; 0.33316809, 0.47785498, 0.0442303' )
        #W_n = W
        #np.fill_diagonal(W,0) # Neurons are not self-connected
        #myW = W # Currently gives in absolute value
        
        # Generates random integer weights for each connection (assuming all-all) w/o self-feedback
        #W = np.random.randint(100, size=[3,3]) 
        W = np.matrix('63, 90, 51; 3, 99, 19; 4, 6, 42')
        W_n = W
        np.fill_diagonal(W,0)
        myW = W

        # Prep for calculation of final weight matrix (basically just replacing all '1' in ADJ mat w/ corresponding WT mat)
        A_temp=np.reshape(A_temp5,(n**2,1)) # Reshapes A for multiplication
        W_temp=np.reshape(W,(1,n**2)) # Reshapes W for multiplication
        W=A_temp * W_temp # Makes (n**2)x(n**2) matrix
        W=W.diagonal() 
        W=np.reshape(W,(n,n)) 

        # Gets the index values for source(rows)/target(cols) neurons 
        rows, cols = np.nonzero(A_temp5)

        # Gets rid of duplicate connections (bidirectional --> unidirectional)
        new_coord = zip(rows,cols)
        #print new_coord
        new_rows = set(tuple(sorted(l)) for l in new_coord)
        g = np.array(list(new_rows))
        rows = g[:,0] #neurons i
        cols = g[:,1] #neurons j
        new_coord = zip(rows,cols)
        
        # To duplicate values above diagonal onto spots below diagonal
        for x in range(len(myW)):
            for y in range(len(myW)):
                myW[y,x] = myW[x,y]

        # Generates weight matrix in array that's necessary for Synapses in Brian2
        # Values are in order of new_coord array
        connect_W = []
        for i in range(len(new_coord)):
            connect_W.append(W[new_coord[i]])
        connect_W = np.array(connect_W) # Weights for each connection in ndarray

        # Changes weights (in array) to inhibitory if coming from an inhibitory neuron (upper 20% of n)
        excit_num = int(0.8*n) # Index for 0:excitatory neurons
        for x in range(len(new_coord)):
            for y in (0,1):
                if new_coord[x][y] >= excit_num: # Any neuron index greater than excit_num is inhibitory
                    connect_W[x] = -connect_W[x]
                    #final_W.append(connect_W(x))

                    
        # Changes weights (in matrix) to inhibitory if coming from an inhibitory neuron (upper 20% of n)
        for x in range(len(myW)):
            for y in range(len(myW)):
                if x >= excit_num:
                    myW[x,y] = -myW[x,y]
                if y >= excit_num:
                    myW[x,y] = -myW[x,y]
        
        return W, rows, cols, connect_W, new_coord, A_temp5, myW


# In[88]:

class Visualization:
    '''
    Function 2: Visualize neural network
    Inputs graph G 
    Returns cluster coefficient & characteristic path length
        & plot of connections between neurons (color-coded)
    For more info: see collective dynamics paper
    
    Description:
    From network model, determines cluster coefficient and characteristic path length for each
        node. For each network, will take average of those values, respectively and yield 
        single integer value.
    From network model, will output plot of connections, color-coded for excitatory and
        inhibitory.
    
    Returns:
        cc_avg: Cluster coefficient averaged over all nodes
        ex_in_plot: Plot of colored excitatory/inhibitory connections
        cpl_avg: Number of edges at shortest path over all nodes 
        
    Parameters:
        G: NetworkX Graph from Function 1
    '''
    def cluster_coeff(self,G):        
        cc = nx.clustering(G)

        cc_y=[]
        for idx in cc:
            cc_y=np.append(cc_y,cc[idx])
        
        cc_avg = np.ndarray.mean(cc_y, dtype=np.float64)
        return cc_avg
    
    def ex_in_connec(self,G,connect_W):
        plt.figure()
        
        edges = G.edges()
        nodes = G.nodes()

        custom_color={}
        for idx in range(len(connect_W)):
            if connect_W[idx] < 0:
                inhib_edge = new_coord[idx]
                G.add_edge(*inhib_edge)
                custom_color[inhib_edge]='b'
            else:
                excit_edge = new_coord[idx]
                G.add_edge(*excit_edge)
                custom_color[excit_edge]='r'
        if 0:
            for idx,idy in enumerate(edges):
                x1,y1 = edges[idx]
                if connect_W < 0:
                    inhib_edge = (x1,y1)
                    G.add_edge(x1,y1)
                    custom_color[x1,y1]='b' # Stores color of edges in dict
                else:
                    excit_edge = (x1,y1)
                    G.add_edge(x1,y1)
                    custom_color[x1,y1]='r'
        
        ex_in_plot=nx.draw_networkx(G,node_color='w',
                         with_labels=True,
                         node_list=nodes,
                         #node_size=50,
                         node_size=200,
                         edge_list=custom_color.keys(),
                         edge_color=custom_color.values(),
                         label='Blue=Inhibitory, Red=Excitatory')
        
    def char_path_len(self,G):
        cpl = nx.all_pairs_shortest_path_length(G)
        my_array = []
        my_key = []
        cpl_count = []
        for idx in cpl:
            myarray = cpl[idx]
            min_val = min(ii for ii in myarray if ii > 0) # Find min length
            for key,length in myarray.iteritems():
                if length == min_val:
                    my_key = np.append(my_key,key)
            my_count = len(my_key) # Find number of edges of that length
            cpl_count = np.append(cpl_count,my_count)
            my_key = []
            cpl_avg = np.mean(cpl_count) # Find average of those edges
        return cpl_avg



# In[89]:

class BrianVisualization:
    '''
    Function 4: Visualization of Brian 
    Define LIF neural population in Brian
    Call to save spike times
    Call to plot voltage monitor
    Call to plot raster plot
    Call to plot histogram
    
    Description:
    Will plot the voltage monitor, raster plot, and histogram of neural network
    
    Returns:
        G: NeuronGroup
        spike_times: Spike times for neuron 0
        all_spikes: Spike times for all neurons
        
    
    Parameters:
        statemon: StateMonitor
        spikemon: SpikeMonitor
        run_time: Simulation run time
    
    '''
    def __init__(self):
        plt.clf() # Clears any previous figures
        
        start_scope()
    
    def LIF_model(self,rows,cols,connect_W):
        eqs = '''
        dv/dt = (I-v)/tau : 1 (unless refractory)
        I : 1
        tau : second
        '''
        
        # Attempt at sinusoidal injection current
        # Each neuron will have different value I_c based on sine function
        if 0:
            I_freq=0.01 #0.01 = 1 period in 100 ms
            I_offset=0 #rad
            I_amp=1 #amp
            tend=run_time+1 #ms
            dt=1 #1 ms time step
            t_curr = np.arange(0, tend, dt)*ms
            tmp = ((I_amp*np.sin(2.0*np.pi*I_freq*t_curr/ms)+I_offset)+3) * uamp
            curr = TimedArray(tmp, dt=dt*ms)
        
        G = NeuronGroup(N, eqs, threshold='v>v_th', reset='v=v_r', refractory=10*ms, method='linear')
        G.v = 0.75 #random so changes dynamics for each neuron --> causes difference in raster
        #G.I = curr(t_curr)/uamp # Sinusoidal current
        G.I = I_c # Constant current 
        G.tau = tau_m * ms
        
        # PoissonInput injection current
        # Each neuron has different input current depending on Poisson distribution
        PI_num = 0.9*N
        subG = G[:] # All neurons stimulated via Poisson
        #subG = G[int(PI_num):] # 10% of total neurons stimulated
        # PoissonInput(target, target_var, N, rate, weight) 
        #P = PoissonInput(subG, 'v', 10, 100*Hz, weight=0.1)
        
        S = Synapses(G, G, 'w : 1', on_pre='v_post += w')
        S.connect(i=rows, j=cols) # Adjacency matrix from Adj.weighted
        S.w = connect_W/float(100) # Weighted matrix (abs value b/c E/I defined in S.connect [see HH_model])
        
        #statemon = StateMonitor(G, 'v', record=True)
        statemon = StateMonitor(G, 'v', record=0) # Records just neuron 0 to save resources
        spikemon = SpikeMonitor(G, variables='v')
        #spikemon_subG = SpikeMonitor(subG, variables='v')

        run(run_time*ms, 'text')
        
        return statemon,spikemon,G#,spikemon_subG
        
    def spike_time(self,spikemon):
        all_values = spikemon.all_values()
        spike_times = all_values['t'][0] # Spike times for just neuron 0
        all_spikes = spikemon.t/ms # Spike times for all neurons
        
        return spike_times,all_spikes
        
    def voltage_monitor(self,statemon):
        plot(statemon.t/ms, statemon.v[0])
        #plot(statemon.t/ms, statemon.v[1])  # Plots second neuron      
        ylabel('Voltage (V)')
        xlabel('Time (ms)')
        
    def raster_plot(self,spikemon):
        plot(spikemon.t/ms, spikemon.i, '.k')
        #plot(spikemon_subG.t/ms, spikemon_subG.i, '.r')
        #plt.show()
        xlabel('Time (ms)')
        ylabel('Neuron index');
        
    def spike_hist(self,run_time,all_spikes):
        my_bins = arange(0,run_time+2,2)
        plt.hist(all_spikes, bins=my_bins)
        xlabel('Time (ms)')
        ylabel('Total number of spikes')


# In[90]:

'''
Sample function 1 calling
Change parameters to fit neural model
Computes weighted adjacency matrix

Parameters to be user-defined:
    n: nodes
    m: edges
    k: neighbor connections
    p: probability 
    d: degrees
'''
# Sample call function
n = 3 # Kills kernel at n = 500, m=1000
m = 3
k = 2
p = 0.2
d = 2
Adj = AdjacencyMatrix(n) # Initiates 

[A,G] = Adj.all_to_all(n) # Defines all-to-all topology
#[A,G] = Adj.random(n,m) # Defines random topology
#[A,G] = Adj.small_world(n,k,p) # Defines small-world topology
#[A,G] = Adj.regular(d,n) # Defines regular topology
#[A,G] = Adj.scale_free(n) # Defines scale-free topology

W,rows,cols,connect_W,new_coord,A_temp5,myW = Adj.Weighted(A,n) # Output

'''
Sample function 2 calling
First plot: cluster coefficient for each neuron
Second plot: excitatory (r) and inhibitory (b) connections
'''
vis = Visualization() # Initiates 
cc_avg = vis.cluster_coeff(G) # Calculates average cluster coefficient

vis.ex_in_connec(G,connect_W) # Plots excitatory/inhibitory connections
cpl_avg = vis.char_path_len(G) # Calculates average characteristic path length
show()
print "Average characteristic path length:"
print cpl_avg


# In[91]:

'''
Sample function 4 calling
Change parameters to fit neural model
Simulates leaky integrate-and-fire neuron model

Parameters:
    N: number of neurons
    tau_m: time constant (ms)
    v_r = reset membrane potential (mv)
    v_th = threshold membrane potential (mv)
    I_c = constant input current
    run_time = simulation time (ms)
'''

N = n 
tau_m = 30
v_r = 0 
v_th = 1 
I_c = 2 
run_time = 1000 #ms

run_t = time.time() # Records initial runtime

BrianVis = BrianVisualization() # Initiates

[statemon,spikemon,G] = BrianVis.LIF_model(rows,cols,connect_W) # Runs LIF model on neurons

elapsed = time.time() - run_t # Calculates final runtime

#[statemon,spikemon,G,spikemon_subG] = BrianVis.LIF_model(rows,cols,connect_W) # Runs LIF model on neurons

[spike_times,all_spikes] = BrianVis.spike_time(spikemon) # Determines spike times

plt.figure(1)
BrianVis.voltage_monitor(statemon) # Plots voltage monitor

plt.figure(2)
BrianVis.raster_plot(spikemon) # Plots raster plot

plt.figure(3)
BrianVis.spike_hist(run_time,all_spikes) # Plots histogram

print 'Total runtime:'
print elapsed # Prints total runtime


# In[94]:

if 0:
    print rows
    print type(rows)
    print cols
    print type(cols)
    print connect_W
    print type(connect_W)
    print A_temp5
    print type(A_temp5)
    print myW
    print type(myW)
    
print A_temp5
print myW
print connect_W/float(100)

