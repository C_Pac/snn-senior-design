
# coding: utf-8

# In[ ]:

'''
Attempt 1 to recreate LIF (excitatory & inhibitory) model to show synchronicity & dynamics.
Draws influence from:

“Sensitivity of noisy neurons to coincident inputs”. 
Rossant C, Leijon S, Magnusson AK, Brette R (2011). Journal of Neuroscience, 31(47)

Page 170 on https://media.readthedocs.org/pdf/brian2/latest/brian2.pdf

Work in progress:
1. Understand difference of Poisson inputs
2. Incorporate parts into LIF_model(?)
''' and None


# In[1]:

from brian2 import *
get_ipython().magic(u'matplotlib inline')


# In[3]:

# neuron parameters
theta = -55*mV
El = -65*mV
vmean = -65*mV
taum = 5*ms
taue = 3*ms
taui = 10*ms
eqs = Equations("""
                dv/dt  = (ge+gi-(v-El))/taum : volt
                dge/dt = -ge/taue : volt
                dgi/dt = -gi/taui : volt
                """)

# input parameters
p = 15
ne = 4000
ni = 1000
lambdac = 40*Hz
lambdae = lambdai = 1*Hz

# synapse parameters
we = .5*mV/(taum/taue)**(taum/(taue-taum))
wi = (vmean-El-lambdae*ne*we*taue)/(lambdae*ni*taui)

# NeuronGroup definition
group = NeuronGroup(N=10, model=eqs, reset='v = El',
                    threshold='v>theta',
                    refractory=5*ms, method='linear')
group.v = El
group.ge = group.gi = 0

# independent E/I Poisson inputs
p1 = PoissonInput(group[0:1], 'ge', N=ne, rate=lambdae, weight=we)
p2 = PoissonInput(group[0:1], 'gi', N=ni, rate=lambdai, weight=wi)

# independent E/I Poisson inputs + synchronous E events
p3 = PoissonInput(group[1:], 'ge', N=ne, rate=lambdae-(p*1.0/ne)*lambdac, weight=we)
p4 = PoissonInput(group[1:], 'gi', N=ni, rate=lambdai, weight=wi)
p5 = PoissonInput(group[1:], 'ge', N=1, rate=lambdac, weight=p*we)

# run the simulation
M = SpikeMonitor(group, variables='v')
SM = StateMonitor(group, 'v', record=True) 
BrianLogger.log_level_info()
run(0.5*second, 'text')

if 0:
    # plot trace and spikes
    for i in [0, 1]:
        spikes = (M.t[M.i == i] - defaultclock.dt)/ms
        val = SM[i].v
        subplot(2,1,i+1)
        plot(SM.t/ms, val)
        plot(tile(spikes, (2,1)), vstack((val[array(spikes, dtype=int)], zeros(len(spikes)))), 'b')
        title("%s: %d spikes/second" % (["uncorrelated inputs", "correlated inputs"][i], M.count[i]))
plt.figure(1)
plot(SM.t/ms, SM.v[0])
xlabel('Voltage (V)')
ylabel('Time (ms)')

plt.figure(2)
plot(SM.t/ms, SM.v[1])
xlabel('Voltage (V)')
ylabel('Time (ms)')

show()
plt.figure(3)
plot(M.t/ms, M.i, '.k')
xlabel('Time (ms)')
ylabel('Neuron index')
show()

