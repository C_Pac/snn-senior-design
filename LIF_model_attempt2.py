
# coding: utf-8

# In[ ]:

'''
Attempt 1 to recreate LIF (excitatory & inhibitory) model to show synchronicity & dynamics.
Draws influence from:

The Leaky Integrate-and-Fire Neuron Model (2012)
Emin Orhan
http://www.cns.nyu.edu/~eorhan/notes/lif-neuron.pdf

Work in progress:
1. Current code is in brian language and may need conversion to Brian2
2. Incorporate parts into LIF_model(?)
''' and None


# In[ ]:

from brian import *
from brian.library.synapses import alpha_synapse
from brian.library.IF import leaky_IF
import numpy as np


# In[ ]:

N_pre = 25 # number of presynaptic neurons
N_post = 1 # number of postsynaptic neurons
tau_m = 10 * ms # membrane time constant
v_r = 0 * mV # reset potential
v_th = 15 * mV # threshold potential
W = 1.62 * mV + 0.5*randn(N_pre,N_post) * mV # synaptic efficacies

# Leaky IF neuron with alpha synapses
eqs = leaky_IF(tau=tau_m,El=v_r)+Current(’I=ge:mV’)+alpha_synapse(input=’I_in’,tau=10*ms,unit=mV,output=’ge’)

lif_pre = PoissonGroup(N_pre, rates=40*Hz)
lif_post = NeuronGroup(N_post, model=eqs, threshold=v_th, reset=v_r)
C = Connection(lif_pre, lif_post, 'ge', weight=W)

spikes_pre = SpikeMonitor(lif_pre)
spikes_post = SpikeMonitor(lif_post)
v_trace = StateMonitor(lif_post, 'vm', record=True)
I_trace = StateMonitor(lif_post, 'ge', record=True)

run(0.1*second, 'text') 
figure(1)
plot(v_trace.times/ms,v_trace[0]/mV)
xlabel(’$\mathrm{Time \; (ms)}$’,fontsize=30)
ylabel(’$\mathrm{v \; (mV)}$’,fontsize=30)

figure(2)
plot(I_trace.times/ms,I_trace[0]/mV)
xlabel(’$\mathrm{Time \; (ms)}$’,fontsize=30)
ylabel(’$\mathrm{I_{post}(t)}$’,fontsize=30)

figure(3)
raster_plot(spikes_pre)
xlabel(’$\mathrm{Time \; (ms)}$’,fontsize=30)
ylabel(’$\mathrm{Neuron \; no}$’,fontsize=30)
show()

