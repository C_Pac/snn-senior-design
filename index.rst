.. Brian2_FPGA documentation master file, created by
   sphinx-quickstart on Sat Apr 22 16:28:26 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Brian2_FPGA's documentation!
=======================================

This is the introduction to this project.

Requirements:

My project depends on Brian2 package.

Contents:

.. toctree::
   :maxdepth: 2

   project
   tutorial
   code


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

