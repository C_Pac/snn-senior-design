
# coding: utf-8

# In[1]:

'''
Deconstructed LIF_module_full to only class functions
'''


# In[ ]:

'''
Dependencies for following classes
'''
import matplotlib.pyplot as plt
import networkx as nx
from brian2 import *
import time
import matplotlib.patches as mpatches
import pyspike as spk
import seaborn as sns


# In[ ]:

class AdjacencyMatrix:  
    '''
    Function 1: Weighted adjacency matrix
    Call to initiate adjacency matrix
    Call to choose which neural network topology with given parameters
    
    Description:
    Given parameters, constructs network with adjacency matrix and applies random weights.
    
    Returns:
        G: NetworkX Graph
        A: Adjacency matrix. Sparse matrix
        rows: Presynaptic neurons
        cols: Postsynaptic neurons
        connect_W: Weights for each E/I connection (in order of rows,cols)
    
    Parameters:
        n: nodes
        m: edges
        k: neighbor connections
        p: probability 
        d: degrees
    '''
    def __init__(self,n): 
        plt.clf() # Clears any previous figures
        plt.close() # Clears any figure windows
        
    def all_to_all(self,n):
        G = nx.complete_graph(n) 
        #nx.draw(G, with_labels=True) # Draws plot with node labels
        #plt.savefig("All to all.png") # If want to save topology image
        A = nx.adjacency_matrix(G) # Outputs unit adjacency matrix
        return A, G

    def random(self,n,m): 
        # Interchangeable based on UI for different types of topography
        G = nx.dense_gnm_random_graph(n,m) # Uses NetX to generate random topography
        #nx.draw(G, with_labels=True) # Draws connectivity figure
        #plt.savefig("Random.png") # Saves connectivity figure as Random.png

        # Extracts ADJACENCY MATRIX from topography and rearranges to manageable array of (n*n) elements
        A = nx.adjacency_matrix(G) # Assigns A as adjacency matrix (which nodes are connected)
        return A, G 
    
    def small_world(self,n,k,p): 
        G = nx.newman_watts_strogatz_graph(n,k,p) 
        #nx.draw(G, with_labels=True)
        #plt.savefig("Small-world.png")
        A = nx.adjacency_matrix(G)
        return A, G
    
    def regular(self,d,n): 
        G = nx.random_regular_graph(d,n)
        #nx.draw(G, with_labels=True)
        #plt.savefig("Regular.png")
        A = nx.adjacency_matrix(G)
        return A, G
    
    def scale_free(self,n): 
        G = nx.scale_free_graph(n) 
        #nx.draw(G, with_labels=True)
        #plt.savefig("Scale free.png")
        A = nx.adjacency_matrix(G)
        return A, G

    def Weighted(self,A,n):
        A_temp1 = A.todense() # Converts A to manageable matrix
        A_temp2 = np.reshape(A_temp1,(1,n**2)) # Reshapes adjacency matrix to array for calculation
        A_temp3 = np.array(A_temp2) # Changes matrix element to type:array for calculation
        A_temp4 = A_temp3[0] # Selects the first and only cell in array for manipulation

        # Generates random values for n neurons to decide whether E/I
        rand_temp=np.random.rand(1,n) 
        rand_temp=rand_temp[0]

        # Changes positive to negative weights based on probability (***not necessary if define E/I in Synapses and subG)
        if 0:
            newlist = [] 
            for item in rand_temp:
                if item > 0.8: # Random numbers to negative according to uniform probability
                    item = -item 
                newlist.append(item) 

        # Reshapes adjacency matrix to workable matrix of n*n neurons
        A_temp5 = np.reshape(A_temp4,(n,n)) 

        # Generates random weights for each connection (assuming all-all) w/o self-feedback
        #W = np.random.rand(n,n) 
        W = np.random.randint(100, size=[n,n])
        W_n = W
        np.fill_diagonal(W,0) # Neurons are not self-connected
        myW = W

        # Prep for calculation of final weight matrix (basically just replacing all '1' in ADJ mat w/ corresponding WT mat)
        A_temp=np.reshape(A_temp5,(n**2,1)) # Reshapes A for multiplication
        W_temp=np.reshape(W,(1,n**2)) # Reshapes W for multiplication
        W=A_temp * W_temp # Makes (n**2)x(n**2) matrix
        W=W.diagonal() 
        W=np.reshape(W,(n,n)) 
        
        # Gets the index values for source(rows)/target(cols) neurons 
        rows, cols = np.nonzero(A_temp5)

        # Gets rid of duplicate connections (bidirectional --> unidirectional)
        new_coord = zip(rows,cols)
        #print new_coord
        new_rows = set(tuple(sorted(l)) for l in new_coord)
        g = np.array(list(new_rows))
        rows = g[:,0] #neurons i
        cols = g[:,1] #neurons j
        new_coord = zip(rows,cols)

        # To duplicate values above diagonal onto spots below diagonal
        for x in range(len(myW)):
            for y in range(len(myW)):
                myW[y,x] = myW[x,y]

        # Generates weight matrix in array that's necessary for Synapses in Brian2
        # Values are in order of new_coord array
        connect_W = []
        for i in range(len(new_coord)):
            connect_W.append(W[new_coord[i]])
        connect_W = np.array(connect_W) # Weights for each connection in ndarray

        # Changes weights (in array) to inhibitory if coming from an inhibitory neuron (upper 20% of n)
        excit_num = int(0.8*n) # Index for 0:excitatory neurons
        for x in range(len(new_coord)):
            for y in (0,1):
                if new_coord[x][y] >= excit_num: # Any neuron index greater than excit_num is inhibitory
                    connect_W[x] = -connect_W[x]
                    #final_W.append(connect_W(x))
                    
        # Changes weights (in matrix) to inhibitory if coming from an inhibitory neuron (upper 20% of n)
        for x in range(len(myW)):
            for y in range(len(myW)):
                if x >= excit_num:
                    myW[x,y] = -myW[x,y]
                if y >= excit_num:
                    myW[x,y] = -myW[x,y]
                    
        # Changing NetworkX variables to float16
        [A_temp5,myW] = np.float16([A_temp5,myW])
        
        #return W, rows, cols, connect_W, new_coord, A_temp5, myW
        return rows,cols,connect_W,A_temp5,myW,new_coord


# In[ ]:

class BrianVisualization:
    '''
    Function 4: Visualization of Brian 
    Define LIF neural population in Brian
    Call to save spike times
    Call to plot voltage monitor
    Call to plot raster plot
    Call to plot histogram
    
    Description:
    Will plot the voltage monitor, raster plot, and histogram of neural network
    
    Returns:
        G: NeuronGroup
        spike_times: Spike times for neuron 0
        all_spikes: Spike times for all neurons
        
    
    Parameters:
        statemon: StateMonitor
        spikemon: SpikeMonitor
        run_time: Simulation run time
    
    '''
    def __init__(self):
        plt.clf() # Clears any previous figures
        plt.close() # Clears any figure windows
        
        start_scope()
    
    def network1(self,rows,cols,connect_W,N,p_0_curr,I_c,refrac_t,tau_m1,PI_onoff1,V_rand1):
        '''
        For full synch: G1.v = fixed
                        PI = off
        '''
        eqs = '''
        dv/dt = ((El-v)+I)/tau : 1 (unless refractory)
        I : 1
        tau : second
        '''
        
        # Changing constant current to 0 current for bottom 10% neurons (http://brian2.readthedocs.io/en/2.0.1/user/models.html#storing-state-variables)
        y = [0]*N # Start with 0 list
        num_0 = p_0_curr*N # Determine 10% of neurons to have 0 current
        y2 = np.array(y) # Change type from list to array
        y2[int(num_0):] = I_c # Change values of last 90% from 0 to I_c
        y3 = list(y2) # Change type from array to list
        initial_values = {'I': y3} 
        
        G1 = NeuronGroup(N, eqs, threshold='v>v_th', reset='v=v_r', refractory=refrac_t*ms, method='linear')
        if V_rand1 == 1:
            G1.v = 'rand()' #random so changes dynamics for each neuron --> changes starting spike
            print 'Random voltage for network 1.\n'
        elif V_rand1 == 0:
            G1.v = '0.967188882214'
            print 'Fixed voltage for network 1.\n'
        #G1.I = I_c # Constant current 
        G1.set_states(initial_values) # Manipulatable current
        G1.tau = tau_m1 * ms
        
        # Save values of neuron parameters
        states = G1.get_states()
        
        # Change to float16
        states_I = np.float16(states['I'])
        states_v = np.float16(states['v'])

        # PoissonInput injection current -- changes each neuron's firing rate (essentially adding noise so irregular firing)
        # Each neuron has different input current depending on Poisson distribution
        PI_num = 0.8*N 
        #subG1 = G1[int(PI_num):] # Top 20% of total neurons stimulated
        subG1 = G1[:] # All neurons stimulated via Poisson 
        '''
        PoissonInput(target,target_var,N,rate,weight)
        target: which neurons to send PoissonInput
        target_var: which variable that is being changed from input
        N: number of inputs (more input = higher firing rate)
        rate: rate of input (100Hz = 10ms per spike)
        weight: amount added to voltage
        '''
        if PI_onoff1 == 1:
            P1 = PoissonInput(subG1, 'v', 5, 100*Hz, weight=0.1) # PoissonInput on
            print 'Poisson input for network 1 on.\n'
        elif PI_onoff1 == 0:
            P1 = PoissonInput(subG1, 'v', 5, 100*Hz, weight=0) # PoissonInput off
            print 'Poisson input for network 1 off.\n'

        S1 = Synapses(G1, G1, 'w : 1', on_pre='v_post += w')
        S1.connect(i=rows, j=cols) # Adjacency matrix from Adj.weighted
        S1.w = connect_W/float(100) # Weighted matrix 
                
        #return G1,S1,P1
        return G1,S1,P1,states_I,states_v
    
    def network2(self,rows,cols,connect_W,N,p_0_curr,I_c,refrac_t,tau_m2,PI_onoff2,V_rand2):
        '''
        Start off w/ identical network parameters as network 1, but need to eventually change connect_W (its interconnections)
        If P2 turned on, may need to increase S3.w so network 1 influence is higher than PoissonInput
        '''
        eqs = '''
        dv/dt = ((El-v)+I)/tau : 1 (unless refractory)
        I : 1
        tau : second
        '''
        # Changing constant current to 0 current for bottom 10% neurons (http://brian2.readthedocs.io/en/2.0.1/user/models.html#storing-state-variables)
        y = [0]*N # Start with 0 list
        num_0 = p_0_curr*N # Determine % of neurons to have 0 current
        y2 = np.array(y) # Change type from list to array
        y2[int(num_0):] = I_c # Change values of last 90% from 0 to I_c
        y3 = list(y2) # Change type from array to list
        initial_values = {'I': y3} 
        
        G2 = NeuronGroup(N, eqs, threshold='v>v_th', reset='v=v_r', refractory=refrac_t*ms, method='linear')
        if V_rand2 == 0:
            G2.v = '0.967188882214' # For debugging of coupling so that all nodes in G2 will fire at same rate
            print 'Fixed voltage for network 2.\n'
        elif V_rand2 == 1:
            G2.v = 'rand()'
            print 'Random voltage for network 2.\n'
        #G2.I = I_c
        G2.set_states(initial_values) # Manipulatable current
        G2.tau = tau_m2 * ms
        
        # Save values of neuron parameters
        states = G2.get_states()

        # Change to float16
        states_I = np.float16(states['I'])
        states_v = np.float16(states['v'])
        
        subG2 = G2[:]
        if PI_onoff2 == 1:
            P2 = PoissonInput(subG2, 'v', 5, 100*Hz, weight=0.1) # PoissonInput on
            print 'Poisson input for network 2 on.\n'
        elif PI_onoff2 == 0:
            P2 = PoissonInput(subG2, 'v', 5, 100*Hz, weight=0) # PoissonInput off
            print 'Poisson input for network 2 off.\n'
        
        S2 = Synapses(G2, G2, 'w:1', on_pre='v_post += w')
        S2.connect(i=rows, j=cols) # Network 2 has same inter-network connections as Network 1
        S2.w = connect_W/float(100)
        
        #return G2,S2,P2
        return G2,S2,P2,states_I,states_v

    def network_coupling(self,N,p_couple,w_couple,G1,G2,run_time,El,v_th,v_r):
        '''
        Should see how coupling between different subpopulation has global effects (raster plot)
            - Could see difference if neurons have same firing rate (non-PoissonInput) vs. different firing rate (all-PoissonInput)
            - May only want to record (Statemon, Spikemon) from this last coupling (G2) to save resources
                - See Monitoring Synaptic Variables from http://brian2.readthedocs.io/en/2.0.1/user/synapses.html
            = Can introduce multiple output synapses (multisynaptic_index from http://brian2.readthedocs.io/en/2.0.1/user/synapses.html)
                - Or more simply "S.connect(i=numpy.arange(10), j=1)"
        '''
        S3 = Synapses(G1,G2, 'w:1', on_pre='v_post += w')#, delay=5*ms) # G1 drives G2
        
        ### Manually defining coupling ###
        p_couple2 = p_couple*N
        i_couple = 0.8*N
        
        # If want 1:1 for only first p_couple% neurons (excitatory --> excitatory)
        c_rows = list(arange(0,p_couple2,dtype=int)) # Source neurons
        c_cols = list(arange(0,p_couple2,dtype=int)) # Target neurons
        
        # If want 1:1 for only last p_couple% neurons 
        #c_rows = list(arange(N-p_couple2,N,dtype=int))
        #c_cols = list(arange(N-p_couple2,N,dtype=int))
        
        # If want 1:1 for inhibitory onto inhibitory neurons
        #c_rows = list(arange(N-i_couple,N,dtype=int))
        #c_cols = list(arange(N-i_couple,N,dtype=int))        
        
        # If want 1:1 for projection of excitatory onto inhibitory neurons
        #c_rows = list(arange(0,p_couple2,dtype=int))
        #c_cols = list(arange(N-i_couple,N,dtype=int))
        
        # If want 1:! for projection of inhibitory onto excitatory neurons
        #c_rows = list(arange(N-p_couple2,N,dtype=int))
        #c_cols = list(arange(0,p_couple2,dtype=int))
        
        S3.connect(i=c_rows, j=c_cols) # Manually defined coupling
        S3.w = w_couple
        ###################################
        
        ##### Probabilistic coupling #####
        #S3.connect(p=0.05) # Probabilistic connection - Chance that G2 will connect with and spike from G1
        #S3.w = 0.02
        #S3.connect(p=p_couple)
        ###################################
                
        # Coupling matrix
        coup_mat = [[0 for x in range(N)] for y in range(N)]

        for ii in range(len(c_rows)):
            for jj in range(len(c_cols)):
                coup_mat[ii][ii] = 1

        statemon1 = StateMonitor(G1, 'v', record=0) # Records just neuron 0 to save resources
        spikemon1 = SpikeMonitor(G1, variables='v')
        statemon2 = StateMonitor(G2, 'v', record=0) # Records just neuron 0 to save resources
        spikemon2 = SpikeMonitor(G2, variables='v')
        
        run_t = time.time() # Records initial runtime
        run(run_time*ms, 'text')
        elapsed = time.time() - run_t # Calculates elapsed runtime
        
        # Changing Brian2 variables to float16
        elapsed = np.float16(elapsed)
        coup_mat = np.float16(coup_mat)

        #return statemon1,spikemon1,statemon2,spikemon2,c_rows,c_cols,coup_mat,elapsed
        return statemon1,spikemon1,statemon2,spikemon2,coup_mat,elapsed
    
    def spike_time(self,spikemon):
        all_values = spikemon.all_values()
        spike_times = all_values['t'][0] # Spike times for just neuron 0
        all_spikes = spikemon.t/ms # Spike times for all neurons
        
        return spike_times,all_spikes
        
    def voltage_monitor(self,statemon):
        plot(statemon.t/ms, statemon.v[0])
        #plot(statemon.t/ms, statemon.v[1])  # Plots second neuron      
        ylabel('Voltage (V)')
        xlabel('Time (ms)')
        
    def raster_plot(self,spikemon,spikemon_other):
        #ion()
        plot(spikemon.t/ms, spikemon.i, '.r')
        plot(spikemon_other.t/ms, spikemon_other.i, '.k') # Plots overlay of each network
        xlabel('Time (ms)')
        ylabel('Neuron index');
        #plt.show(block=True)
        
    def spike_hist(self,run_time,all_spikes):
        my_bins = arange(0,run_time+2,2)
        plt.hist(all_spikes, bins=my_bins)
        xlabel('Time (ms)')
        ylabel('Total number of spikes')


# In[ ]:

class Visualization:
    '''
    Function 2: Visualize neural network
    Inputs graph G 
    Returns cluster coefficient & characteristic path length
        & plot of connections between neurons (color-coded)
    For more info: see collective dynamics paper
    
    Description:
    From network model, determines cluster coefficient and characteristic path length for each
        node. For each network, will take average of those values, respectively and yield 
        single integer value.
    From network model, will output plot of connections, color-coded for excitatory and
        inhibitory.
    
    Returns:
        cc_avg: Cluster coefficient averaged over all nodes
        ex_in_plot: Plot of colored excitatory/inhibitory connections
        cpl_avg: Number of edges at shortest path over all nodes 
        
    Parameters:
        G: NetworkX Graph from Function 1
    '''
    def __init__(self):
        #plt.clf() # Clears any previous figures
        plt.close() # Clears any figure windows

    def cluster_coeff(self,G):        
        cc = nx.clustering(G)

        cc_y=[]
        for idx in cc:
            cc_y=np.append(cc_y,cc[idx])
        
        cc_avg = np.ndarray.mean(cc_y, dtype=np.float64)
        return cc_avg
    
    def ex_in_connec(self,G,connect_W,new_coord):
        plt.figure()
        red_patch = mpatches.Patch(color='red', label='Excitatory')
        blue_patch = mpatches.Patch(color='blue', label='Inhibitory')
        plt.legend(handles=[red_patch,blue_patch])
        suptitle('Structural Connections', fontsize=14, fontweight='bold')

        edges = G.edges()
        nodes = G.nodes()

        custom_color={}
        for idx in range(len(connect_W)):
            if connect_W[idx] < 0:
                inhib_edge = new_coord[idx]
                G.add_edge(*inhib_edge)
                custom_color[inhib_edge]='b'
            else:
                excit_edge = new_coord[idx]
                G.add_edge(*excit_edge)
                custom_color[excit_edge]='r'
        if 0:
            for idx,idy in enumerate(edges):
                x1,y1 = edges[idx]
                if connect_W < 0:
                    inhib_edge = (x1,y1)
                    G.add_edge(x1,y1)
                    custom_color[x1,y1]='b' # Stores color of edges in dict
                else:
                    excit_edge = (x1,y1)
                    G.add_edge(x1,y1)
                    custom_color[x1,y1]='r'
        
        ex_in_plot=nx.draw_networkx(G,node_color='w',
                         with_labels=True,
                         node_list=nodes,
                         #node_size=50,
                         node_size=200,
                         edge_list=custom_color.keys(),
                         edge_color=custom_color.values(),
                         label='Blue=Inhibitory, Red=Excitatory')
        #plt.savefig("Structural Connections.png")
        
    def char_path_len(self,G):
        cpl = nx.all_pairs_shortest_path_length(G)
        my_array = []
        my_key = []
        cpl_count = []
        for idx in cpl:
            myarray = cpl[idx]
            min_val = min(ii for ii in myarray if ii > 0) # Find min length
            for key,length in myarray.iteritems():
                if length == min_val:
                    my_key = np.append(my_key,key)
            my_count = len(my_key) # Find number of edges of that length
            cpl_count = np.append(cpl_count,my_count)
            my_key = []
            cpl_avg = np.mean(cpl_count) # Find average of those edges
        return cpl_avg


# In[ ]:

class SynchronicityCalculation:
    '''
    To calculate different metrics of synchronicity
    
    For more information:
        See Synch Metrics bookmarks folder
        http://wwwold.fi.isc.cnr.it/users/thomas.kreuz/sourcecode.html
        https://arxiv.org/pdf/1603.03293.pdf
        http://mariomulansky.github.io/PySpike/pyspike.html#pyspike.SpikeTrain.SpikeTrain
        http://mariomulansky.github.io/PySpike/index.html
        http://www.scholarpedia.org/article/Measures_of_spike_train_synchrony#ISI-distance
    '''
    def __init__(self):
        plt.clf() # Clears any previous figures
        plt.close() # Clears any figure windows

    def Initialize(self,spikemon1,spikemon2,run_time):
        # Get spike train data from SpikeMonitor that is ordered by neuron index
        spike_trains1 = spikemon1.spike_trains()
        spike_trains2 = spikemon2.spike_trains()

        # Convert spike trains for each population to list
        list_spike_train1 = [ v for v in spike_trains1.values() ]
        list_spike_train2 = [ v for v in spike_trains2.values() ] # Further differentiate into spike trains for indiv. neuron?

        # Change units to ms
        test1 = [(x/second)*1000 for x in list_spike_train1]
        test2 = [(x/second)*1000 for x in list_spike_train2]

        x = []
        z = []
        for jj in range(len(test1)):
            for ii in range(len(test1[jj])):
                x.append(test1[jj][ii],)
            z.append(x)
            x = []

        spike_train1_txt = open('Spikes1_testing.txt','w')
        for ii in range(len(z)):
            for item in z[ii]:
                spike_train1_txt.write("%s " % item)
            spike_train1_txt.write("\n")

        x = []
        z = []
        for jj in range(len(test2)):
            for ii in range(len(test2[jj])):
                x.append(test2[jj][ii],)
            z.append(x)
            x = []

        spike_train2_txt = open('Spikes2_testing.txt','w')
        for ii in range(len(z)):
            for item in z[ii]:
                spike_train2_txt.write("%s " % item)
            spike_train2_txt.write("\n")

        x = []
        z = []
        for jj in range(len(test1)):
            for ii in range(len(test1[jj])):
                x.append(test1[jj][ii],)
            z.append(x)
            x = []
            for ii in range(len(test2[jj])):
                x.append(test2[jj][ii],)
            z.append(x)
            x = []    

        spike_train12_txt = open('Spikes12_testing.txt','w')
        for ii in range(len(z)):
            for item in z[ii]:
                spike_train12_txt.write("%s " % item)
            spike_train12_txt.write("\n")

        # Load spikes from saved txt file
        my_st1 = spk.load_spike_trains_from_txt("Spikes1_testing.txt",edges=[0,run_time])
        my_st2 = spk.load_spike_trains_from_txt("Spikes2_testing.txt",edges=[0,run_time])
        my_st12 = spk.load_spike_trains_from_txt("Spikes12_testing.txt",edges=[0,run_time])
        
        return my_st1,my_st2,my_st12

    def Metrics(self,compare_me,compare_me_spikes,reference_spikes,run_time,BrianVis):
        # PSTH source
        [spike_times,all_spikes] = BrianVis.spike_time(reference_spikes)
        fig1 = plt.subplot(511)
        suptitle('Synchronicity Metrics', fontsize=14, fontweight='bold')
        BrianVis.spike_hist(run_time,all_spikes)
        xlabel('')
        
        # PSTH target
        [spike_times,all_spikes] = BrianVis.spike_time(compare_me_spikes)
        fig1 = plt.subplot(512)
        BrianVis.spike_hist(run_time,all_spikes)
        xlabel('')

        # Raster plot
        if 0:
            fig1 = plt.subplot(411)
            suptitle('Synchronicity Metrics', fontsize=14, fontweight='bold')
            BrianVis.raster_plot(spikemon2,spikemon1)
                # Red == Network2 (target network)
                # Black == Network1 (source network)

        # Gather ISI profile
        isi_prof = spk.isi_profile(compare_me) # Comparing entire intra-population
        #isi_prof = spk.isi_profile(my_st1[0],my_st2[0]) # The neurons that aren't spiking are ignored -- see my_st1[0][0]
        isi_dist = isi_prof.avrg()
        x,y = isi_prof.get_plottable_data()

        # Plot of ISI-distance
        fig2 = plt.subplot(513, sharex=fig1)
        plot(x,y,'-k')
        ylabel('ISI')

        # Gather SPIKE-distance
        spike_dist = spk.spike_distance(compare_me)
        spike_profile = spk.spike_profile(compare_me)
        x,y = spike_profile.get_plottable_data()

        # Plotting SPIKE-distance
        fig4 = plt.subplot(514, sharex=fig1)
        plot(x,y,'-k')
        ylabel('SPIKE-dist')
            
        # Gather SPIKE-synchronicity
        spike_sync = spk.spike_sync(compare_me)
        spike_profile = spk.spike_sync_profile(compare_me)
        x,y = spike_profile.get_plottable_data()

        # Plotting SPIKE-synchronicity
        fig3 = plt.subplot(515, sharex=fig1)
        plot(x,y,'-k')
        ylabel('SPIKE-sync')
        xlabel('Time (ms)')
        
    def CrossCorrelation(self,spikemon1,spikemon2,bin_time,run_time):
        '''
        Is it necessary to have binary vectors for each indiv. neuron like CorrelationMatrix?
        '''
        #my_bins = arange(0,run_time+2,2) # 2 ms bins
        #my_bins = arange(0,run_time+1,1) # 1 ms bins
        #my_bins = arange(0,run_time+0.05,0.05) # 0.05 ms bins
        my_bins = arange(0,run_time+bin_time,bin_time) # 0.2 ms bins
        
        # Extract binary vector where 1 if spike within given time bin, else 0
        my_spikes1 = spikemon1.t/ms
        plt.figure(10)
        n1 = plt.hist(my_spikes1, bins=my_bins)
        my_vec1 = n1[0]
        for ii in range(len(my_vec1)):
            if my_vec1[ii] >= 1:
                my_vec1[ii] = 1

        my_spikes2 = spikemon2.t/ms #+ 100
        plt.figure(11)
        n2 = plt.hist(my_spikes2, bins=my_bins)
        my_vec2 = n2[0]
        for ii in range(len(my_vec2)):
            if my_vec2[ii] >= 1:
                my_vec2[ii] = 1 
        
        plt.close(10)
        plt.close(11)
        #plt.clf() # Clears histogram plots

        # Normalize spike times
        norm1 = my_vec1 / np.linalg.norm(my_vec1)
        norm2 = my_vec2 / np.linalg.norm(my_vec2)
        my_vec_norm1 = norm1
        my_vec_norm2 = norm2

        # Find correlation
        coup_corr = np.correlate(my_vec_norm1,my_vec_norm2,"full") 
        self_corr = np.correlate(my_vec_norm2,my_vec_norm2,"full") 
    
        # Plotting correlation
        x_val_coup_corr = range(len(coup_corr))
        x_val_self_corr = range(len(self_corr))
        #plot(x_val_coup_corr-np.argmax(self_corr/ms),coup_corr,'b') # Shifted
        #plot(x_val_self_corr-np.argmax(self_corr/ms),self_corr,'g') # Shifted
        plot(x_val_coup_corr,coup_corr,'b') # Non-shifted
        plot(x_val_self_corr,self_corr,'g') # Non-shifted
        blue_patch = mpatches.Patch(color='blue', label='Test Correlation: net1-net2')
        green_patch = mpatches.Patch(color='green', label='Autocorrelation: net2-net2')
        suptitle('Comparing networks', fontsize=14, fontweight='bold')
        plt.legend(handles=[blue_patch,green_patch])
        
    def CorrelationMatrix(self,spikemon1,spikemon2,bin_time,run_time):
        '''
        Takes spike train for each individual neuron into array w/ n rows (instead of two vectors for two populations of length m spikes)
        Making the binary vectors take a long time (bin_time=2, N>20, random)
        '''
        my_bins = arange(0,run_time+bin_time,bin_time)*ms

        # Get spike train data from SpikeMonitor that is ordered by neuron index
        spike_trains1 = spikemon1.spike_trains()
        spike_trains2 = spikemon2.spike_trains()

        # Convert spike trains for each population to list
        list_spike_train1 = [ v for v in spike_trains1.values() ]
        list_spike_train2 = [ v for v in spike_trains2.values() ] # Further differentiate into spike trains for indiv. neuron?

        array_spike_train1 = np.array(list_spike_train1)
        array_spike_train2 = np.array(list_spike_train2)

        # Generalized binary vectors for entire array_spike_train1
        binary_vec1 = [None]*len(array_spike_train1) # Initialization of variable

        for jj in range(len(array_spike_train1)):
            my_spikes1 = array_spike_train1[jj] # Looking at individual neurons
            plt.figure(10)
            n1 = plt.hist(my_spikes1, bins=my_bins)
            my_vec1 = n1[0] # Values of histogram bins for each neuron
            for ii in range(len(my_vec1)):
                if my_vec1[ii] >= 1:
                    my_vec1[ii] = 1
            binary_vec1[jj] = my_vec1

        # Generalized binary vectors for entire array_spike_train2
        binary_vec2 = [None]*len(array_spike_train2)

        for jj in range(len(array_spike_train2)):
            my_spikes2 = array_spike_train2[jj]
            plt.figure(10)
            n2 = plt.hist(my_spikes2, bins=my_bins)
            my_vec2 = n2[0]
            for ii in range(len(my_vec2)):
                if my_vec2[ii] >= 1:
                    my_vec2[ii] = 1
            binary_vec2[jj] = my_vec2

        plt.close(10)
        R = np.corrcoef(binary_vec1,binary_vec2) # Numpy correlation coefficient matrix
        f,ax = plt.subplots(figsize=(12,9))
        my_corr = sns.heatmap(R,vmin=0,vmax=1,square=True)
        ax.set_title('Pairwise Correlation Heatmap')
        f.tight_layout()
        
        #return binary_vec1, binary_vec2

