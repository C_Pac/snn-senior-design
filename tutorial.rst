Sample simulation tutorial
==========================
        
Dependencies::
    
    # Not needed b/c in LIF_C: import networkx as nx
    import matplotlib.pyplot as plt
    # Not needed b/c in LIF_C: import matplotlib.patches as mpatches
    import numpy as np
    import time
    # Not needed b/c in LIF_C: from brian2 import *
    # http://brian2.readthedocs.io/en/stable/introduction/install.html
    '''
    from brian2 import start_scope
    from brian2 import NeuronGroup
    from brian2 import Synapses
    from brian2 import SpikeMonitor
    from brian2.units import *
    from brian2 import run
    '''
    # Not needed b/c in LIF_C: import pyspike as spk # Necessary for plotting synch metrics (ISI-distance,SPIKE-distance,SPIKE-synchronicity)
    # Not needed b/c in LIF_C: import seaborn as sns # Necessary for plotting pairwise correlation
    #%matplotlib inline 
    import LIF_Classes as LIF_C # Needs to connect to custom LIF_Classes

Setting up constants::

    '''

    Global variables:
        n: nodes
        m: edges
        k: each node is connected to k nearest neightbors
        p: probability of adding new edge for each edge
        d: degree of each node
        N: number of neurons
        tau_m: time constant (ms)
        v_r = reset membrane potential (mv)
        v_th = threshold membrane potential (mv)
        I_c = constant input current
        El = Resting leaky potential
        refrac_t = refractory time (ms)
        run_time = simulation time (ms)
        p_couple = probability that neuron i in first net will couple with neuron i in second net
        w_couple = coupling weight (influence from net1 to net2)
        p_0_curr = percentage of network that has 0 injection current

    Network topology variables:
        new_coord: necessary for network connection visualization
        Adj_mat: Adjacency matrix
        W_mat: Weighted adjacency matrix
        Coup_mat: Coupling matrix
        states1['I']: Network 1 injection current
        states1['v']: Network 1 voltage
        states2['I']: Network 2 injection current
        states2['v']: Network 2 voltage
    '''

    # Global variables to define network topolgoy
    n = 200
    m = 350
    k = 2
    p = 0.8
    d = 2
    rand_seed = np.random.seed(int(time.time())) # To seed random number generator based on time

    # Global variables to run Brian2 simulation
    N = n 
    tau_m1 = 37 #20.4
    tau_m2 = 43 #32.4 
    v_r = 0 
    v_th = 1 
    I_c = 2 
    El = 1
    refrac_t = 10
    run_time = 600
    p_couple = 0.5 #Good:0.5 #Good:0.2 # 0.99 
    w_couple = 0.5 #Good:0.3 #Good:0.5 #1 
    p_0_curr = 0.1

    '''
    Manipulatable variables that are hidden in LIF_Classes source code:
        PI_onoff1 : 0 if want PI off for network 1
        PI_onoff1 : 1 if want PI on for network 1
        PI_onoff2 : 0 if want PI off for network 2
        PI_onoff2 : 1 if want PI on for network 2   

        V_rand1 : 0 if want fixed voltage for network 1
        V_rand1 : 1 if want random voltage for network 1
        V_rand2 : 0 if want fixed voltage for network 2
        V_rand2 : 1 if want random voltage for network 2

    '''
    #Full synchronous if PI_onoff1 = 0 and V_rand1 = 0 
    PI_onoff1 = 1
    V_rand1 = 1
    PI_onoff2 = 1
    V_rand2 = 1
    
Defining network topology::

    Adj = LIF_C.AdjacencyMatrix(n) # Initiates 
    #[A,G] = Adj.all_to_all(n) # Defines all-to-all topology
    [A,G] = Adj.random(n,m) # Defines random topology
    #[A,G] = Adj.small_world(n,k,p) # Defines small-world topology
    #[A,G] = Adj.regular(d,n) # Defines regular topology
    #[A,G] = Adj.scale_free(n) # Defines scale-free topology
    [rows,cols,connect_W,Adj_mat,W_mat,new_coord] = Adj.Weighted(A,n) # Output
    #[W,rows,cols,connect_W,new_coord,Adj_mat,W_mat] = Adj.Weighted(A,n) # Output

Running Brian2 simulation::

    BrianVis = LIF_C.BrianVisualization() # Initiates
    [G1,S1,P1,states1_I,states1_v] = BrianVis.network1(rows,cols,connect_W,N,p_0_curr,I_c,refrac_t,tau_m1,PI_onoff1,V_rand1) # Runs LIF model for first network
    [G2,S2,P2,states2_I,states2_v] = BrianVis.network2(rows,cols,connect_W,N,p_0_curr,I_c,refrac_t,tau_m2,PI_onoff2,V_rand2) # Runs LIF model for second network
    [statemon1,spikemon1,statemon2,spikemon2,Coup_mat,elapsed] = BrianVis.network_coupling(N,p_couple,w_couple,G1,G2,run_time,El,v_th,v_r) # Couples first and second networks
    print 'Total runtime:'
    print elapsed # Prints elpased runtime

Plotting network dynamics (raster and PSTH)::

    BrianVis = LIF_C.BrianVisualization() # Initiates

    # Gather data for network 1
    [spike_times,all_spikes] = BrianVis.spike_time(spikemon1)

    # Network 1 raster
    fig1 = plt.subplot(221)
    plt.suptitle('Two network raster and PSTH', fontsize=14, fontweight='bold')
    BrianVis.raster_plot(spikemon1,spikemon1) 
        # Network 1 in black (second input)

    # Network 1 PSTH
    fig2 = plt.subplot(222, sharex=fig1)
    BrianVis.spike_hist(run_time,all_spikes) 

    # Gather data for network 2
    [spike_times,all_spikes] = BrianVis.spike_time(spikemon2)

    # Network 2 raster
    fig3 = plt.subplot(223, sharex=fig1, sharey=fig1)
    #BrianVis.raster_plot(spikemon2,spikemon1) 
        # Network 2 in red (first input)
        # Network 1 in black (second input)
    BrianVis.raster_plot(spikemon2,spikemon2) 

    # Network 2 PSTH
    fig4 = plt.subplot(224, sharex=fig1)
    BrianVis.spike_hist(run_time,all_spikes)

    # Qt4Agg backend for full screen window display
    figManager = plt.get_current_fig_manager()
    figManager.window.showMaximized()

    plt.show(block=True)

Plotting network connections and calculating characteristic path length::

    vis = LIF_C.Visualization() # Initiates 
    cc_avg = vis.cluster_coeff(G) # Calculates average cluster coefficient

    vis.ex_in_connec(G,connect_W,new_coord) # Plots excitatory/inhibitory connections
    #cpl_avg = vis.char_path_len(G) # Calculates average characteristic path length
    #print "Average characteristic path length:"
    #print cpl_avg

    plt.show(block=True) # Show Network connections

Plotting synchronicity metrics::

    BrianVis = LIF_C.BrianVisualization()
    #if 0: # Not showing b/c testing network definition for FPGA implementation

    Sync = LIF_C.SynchronicityCalculation() # Initiates
    [my_st1,my_st2,my_st12] = Sync.Initialize(spikemon1,spikemon2,run_time)

    # User inputs
    compare_me = my_st12
    compare_me_spikes = spikemon2
    reference_spikes = spikemon1

    Sync.Metrics(compare_me,compare_me_spikes,reference_spikes,run_time,BrianVis)

    figManager = plt.get_current_fig_manager()
    figManager.window.showMaximized()

    plt.show(block=True)

Plotting pairwise correlation::

    bin_time = 2 # 2

    Sync = LIF_C.SynchronicityCalculation() # Initiates
    Sync.CorrelationMatrix(spikemon1,spikemon2,bin_time,run_time)

    # Organization of correlation
    # Top left: intra 1
    # Bottom right: intra 2
    # Top right: inter 2-1
    # Bottom left: inter 1-2

    plt.show(block=True)

Plotting cross-correlation::

    bin_time = 0.2 # 0.2 ms bins

    Sync = LIF_C.SynchronicityCalculation() # Initiates

    plt.suptitle('Correlations Between Networks', fontsize=14, fontweight='bold')

    # If want overlaid plots -- Need to de-comment the self_corr from function
    Sync.CrossCorrelation(spikemon1,spikemon2,bin_time,run_time)

    figManager = plt.get_current_fig_manager()
    figManager.window.showMaximized()

    plt.show(block=True)
    
Call function to get variables of interest for FPGA transfer::

    '''
    Variables:
        El: v_rest
        tau_m1: network 1 membrane time constant
        tau_m2: network 2 membrane time constant
        v_th: v_threshold
        refrac_t: refractory time
        v_r: v_reset
        intranet1_wt: network 1 bias
        intranet2_wt: network 2 bias
        w_couple: network 1-2 bias
        W1: network 1 weight matrix
        W2: network 2 weight matrix
        W12: network 1-2 weight matrix
    '''
    # Network paramters
    print 'V_rest = %f' % El
    #print 'Tau_syn = %f' %
    #print 'Tau_mem = %f' %
    print 'V_thr = %f' % v_th
    print 'Refractory time = %f' % refrac_t
    print 'V_reset = %f' % v_r
    print 'Intra-network bias:'
    print W_mat

    # Inter-network weight matrix (rows = source, cols = target)
    W12 = np.zeros((N,N),dtype=np.float16)
    for ii in range(len(Coup_mat)):
        for jj in range(len(Coup_mat)):
            if Coup_mat[ii][jj] == 1:
                W12[ii][jj] = Coup_mat[ii][jj]*w_couple
    print 'Inter-network weight matrix:'
    print W12

    # If heterogeneous weights
    if 0: 
        # Intra-network weight matrix (rows = source, cols = target)
        W1 = np.zeros((N,N))
        for (ii,jj) in new_coord1_test:
            W1[ii][jj] = intranet1_wt
        print 'Intra-network 1 weight matrix:'
        print W1

        W2 = np.zeros((N,N))
        for (ii,jj) in new_coord2_test:
            W2[ii][jj] = intranet2_wt
        print 'Intra-network 2 weight matrix:'
        print W2
